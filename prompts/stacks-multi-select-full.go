package prompts

import (
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
)

func StacksMultiSelectFull(app App, cmd *cobra.Command, message string, yesFlag string) []string {

	cfg := app.GetConfig()
	var stackNames []string

	if cfg.Stack != "" {
		stackNames = append(stackNames, cfg.Stack)
	}

	if len(stackNames) == 0 {
		allStackNames := compose.GetStackNames(app)

		var yes bool
		if yesFlag != "" {
			yes, _ = cmd.Flags().GetBool(yesFlag)
		}

		if yes {
			stackNames = allStackNames
		} else {
			stackNames = StacksMultiSelectWithAll(allStackNames, message)
		}

	}
	return stackNames
}
