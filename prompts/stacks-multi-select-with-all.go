package prompts

import (
	"fmt"

	survey "github.com/AlecAivazis/survey/v2"
)

func StacksMultiSelectWithAll(allStackNames []string, message string) []string {
	var all string
	prompt := &survey.Select{
		Message: fmt.Sprintf(message, allStackNames),
		Options: []string{"yes", "no", "cancel"},
		Default: "no",
	}
	survey.AskOne(prompt, &all)

	var stackNames []string

	if all == "yes" {
		stackNames = allStackNames
	} else if all == "no" {
		stackNames = StacksMultiSelect(allStackNames)
	}

	return stackNames
}
