package prompts

import (
	survey "github.com/AlecAivazis/survey/v2"
)

func StacksMultiSelect(allStackNames []string) []string {
	selectedStacks := []string{}
	prompt := &survey.MultiSelect{
		Message: "Select stacks:",
		Options: allStackNames,
	}
	survey.AskOne(prompt, &selectedStacks)
	return selectedStacks
}
