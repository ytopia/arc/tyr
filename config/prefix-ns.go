package config

func PrefixNS(name string, namespace string) string {
	if namespace != "" {
		return namespace + "_" + name
	}
	return name
}
