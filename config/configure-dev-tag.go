package config

import (
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/goenv"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func (cl *ConfigLoader) configureDevTag() {
	cfg := cl.Config
	cfg.MachineID = tools.GetDefaultMachineId()

	envMap := make(map[string]string)
	envMap[cl.PrefixEnv("MACHINE_ID")] = cfg.MachineID
	var err error
	var devTag string
	devTag, err = goenv.Expand(cfg.DevTag, envMap)
	errors.Check(err)

	cfg.DevTag = devTag

	if cfg.ServiceDevTag == "true" || (cfg.ServiceDevTag == "auto" && tools.SliceContainsString(cfg.Env, "dev")) {
		cfg.ServiceDevTagEnabled = true
	} else {
		cfg.ServiceDevTagEnabled = false
	}
}
