package config

func (cl *ConfigLoader) configureNamespace() {
	cfg := cl.Config
	if cfg.StackNamespace == "" {
		cfg.StackNamespace = cfg.Namespace
	}
	if cfg.NetworkNamespace == "" {
		cfg.NetworkNamespace = cfg.Namespace
	}
	if cfg.VolumeNamespace == "" {
		cfg.VolumeNamespace = cfg.Namespace
	}
	if cfg.SecretNamespace == "" {
		cfg.SecretNamespace = cfg.Namespace
	}
	if cfg.ConfigNamespace == "" {
		cfg.ConfigNamespace = cfg.Namespace
	}

	if cfg.StackNamespace == "-" {
		cfg.StackNamespace = ""
	}
	if cfg.NetworkNamespace == "-" {
		cfg.NetworkNamespace = ""
	}
	if cfg.VolumeNamespace == "-" {
		cfg.VolumeNamespace = "-"
	}
	if cfg.SecretNamespace == "-" {
		cfg.SecretNamespace = ""
	}
	if cfg.ConfigNamespace == "-" {
		cfg.ConfigNamespace = ""
	}
}
