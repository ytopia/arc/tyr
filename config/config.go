package config

import (
	"io/ioutil"
	"os"
	"time"

	survey "github.com/AlecAivazis/survey/v2"
	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

// NewConfig create a Config
func NewConfig() *Config {
	config := &Config{}
	return config
}

// Config struct
type Config struct {
	LogLevel         string `mapstructure:"LOG_LEVEL" json:"log_level"`
	LogType          string `mapstructure:"LOG_TYPE" json:"log_type"`
	LogForceColors   bool   `mapstructure:"LOG_FORCE_COLORS" json:"logs_force_colors"`
	CWD              string `mapstructure:"CWD" json:"cwd"`
	Namespace        string `mapstructure:"NAMESPACE" json:"namespace"`
	StackNamespace   string `mapstructure:"STACK_NAMESPACE" json:"stack_namespace"`
	NetworkNamespace string `mapstructure:"NETWORK_NAMESPACE" json:"network_namespace"`
	VolumeNamespace  string `mapstructure:"VOLUME_NAMESPACE" json:"volume_namespace"`
	SecretNamespace  string `mapstructure:"SECRET_NAMESPACE" json:"secret_namespace"`
	ConfigNamespace  string `mapstructure:"CONFIG_NAMESPACE" json:"config_namespace"`

	MachineID     string   `mapstructure:"MACHINE_ID" json:"machine_id"`
	DevTag        string   `mapstructure:"DEV_TAG" json:"dev_tag"`
	Env           []string `mapstructure:"ENV" json:"env"`
	ServiceDevTag string   `mapstructure:"SERVICE_DEV_TAG" json:"service_dev_tag"`

	BuildComposeFile string   `mapstructure:"BUILD_COMPOSE_FILE" json:"build_compose_file"`
	BuildParameters  []string `mapstructure:"BUILD_PARAMETERS" json:"build_parameters"`
	BuildDeps        bool     `mapstructure:"BUILD_DEPS" json:"build_deps"`
	BuildMissingDeps bool     `mapstructure:"BUILD_MISSING_DEPS" json:"build_missing_deps"`
	BuildCleanEnv    bool     `mapstructure:"BUILD_CLEAN_ENV" json:"build_clean_env"`

	BuildsDir        string `mapstructure:"BUILDS_DIR" json:"builds_dir"`
	DeployDir        string `mapstructure:"DEPLOY_DIR" json:"deploy_dir"`
	SharedFile       string `mapstructure:"SHARED_FILE" json:"shared_file"`
	SharedEnvDir     string `mapstructure:"SHARED_ENV_DIR" json:"shared_env_dir"`
	DeployBundlesDir string `mapstructure:"DEPLOY_BUNDLES_DIR" json:"deploy_bundles_dir"`
	DeployLocksDir   string `mapstructure:"DEPLOY_LOCKS_DIR" json:"deploy_locks_dir"`
	ComposeVersion   string `mapstructure:"COMPOSE_VERSION" json:"compose_version"`

	DeployPullCheck        bool `mapstructure:"DEPLOY_PULL_CHECK" json:"deploy_pull_check"`
	DeployWithRegistryAuth bool `mapstructure:"DEPLOY_WITH_REGISTRY_AUTH" json:"deploy_with_registry_auth"`
	DeployPrune            bool `mapstructure:"DEPLOY_PRUNE" json:"deploy_prune"`

	CryptoKey     string `mapstructure:"CRYPTO_KEY" json:"crypto_key"`
	CryptoKeyFile string `mapstructure:"CRYPTO_KEY_FILE" json:"crypto_key_file"`

	RandomSize   int    `mapstructure:"RANDOM_SIZE" json:"random_size"`
	RandomPrefix string `mapstructure:"RANDOM_PREFIX" json:"random_prefix"`
	RandomRegex  string `mapstructure:"RANDOM_REGEX" json:"random_regex"`
	RandomChars  string `mapstructure:"RANDOM_CHARS" json:"random_chars"`
	RandomLock   bool   `mapstructure:"RANDOM_LOCK" json:"random_lock"`

	RotateRandomInterval       time.Duration `mapstructure:"ROTATE_RANDOM_INTERVAL" json:"rotate_random_interval"`
	RotateRandomForce          bool          `mapstructure:"ROTATE_RANDOM_FORCE" json:"rotate_random_force"`
	RotateRandomForceLock      bool          `mapstructure:"ROTATE_RANDOM_FORCE_LOCK" json:"rotate_random_force_lock"`
	RotateRandomUpdateServices bool          `mapstructure:"ROTATE_RANDOM_UPDATE_SERVICES" json:"rotate_random_update_services"`
	CreateSecretDisplay        bool          `mapstructure:"CREATE_SECRET_DISPLAY" json:"create_secret_display"`

	EncryptData string `mapstructure:"ENCRYPT_DATA" json:"encrypt_data"`
	DecryptData string `mapstructure:"DECRYPT_DATA" json:"decrypt_data"`

	Stack string `mapstructure:"STACK" json:"stack"`

	BuildsRootDir string `mapstructure:"BUILDS_ROOT_DIR" json:"builds_root_dir"`

	ShBash bool `mapstructure:"SH_BASH" json:"sh_bash"`

	Yes                    bool     `mapstructure:"YES" json:"yes"`
	CleanConfigsKeepLatest bool     `mapstructure:"CLEAN_CONFIGS_KEEP_LATEST" json:"clean_configs_keep_latest"`
	CleanSecretsKeepLatest bool     `mapstructure:"CLEAN_SECRETS_KEEP_LATEST" json:"clean_secrets_keep_latest"`
	LogsFollow             bool     `mapstructure:"LOGS_FOLLOW" json:"logs_follow"`
	LogsGrep               string   `mapstructure:"LOGS_GREP" json:"logs_grep"`
	LogsGrepOpts           []string `mapstructure:"LOGS_GREP_OPTS" json:"logs_grep_opts"`
	Sucmd                  string   `mapstructure:"SUCMD" json:"sucmd"`
	BundleUpdateImage      bool     `mapstructure:"BUNDLE_UPDATE_IMAGE" json:"bundle_update_image"`
	BundleUpdateStack      bool     `mapstructure:"BUNDLE_UPDATE_STACK" json:"bundle_update_stack"`
	RmDetach               bool     `mapstructure:"RM_DETACH" json:"rm_detach"`
	RmVolumes              bool     `mapstructure:"RM_VOLUMES" json:"rm_volumes"`
	PullForce              bool     `mapstructure:"PULL_FORCE" json:"pull_force"`
	PullIgnoreTags         []string `mapstructure:"PULL_IGNORE_TAGS" json:"pull_ignore_tags"`
	PullIgnoreDevTag       bool     `mapstructure:"PULL_IGNORE_DEV_TAG" json:"pull_ignore_dev_tag"`
	UpDetach               bool     `mapstructure:"UP_DETACH" json:"up_detach"`
	UpPull                 bool     `mapstructure:"UP_PULL" json:"up_pull"`
	UpBuild                bool     `mapstructure:"UP_BUILD" json:"up_build"`
	UpBuildMissing         bool     `mapstructure:"UP_BUILD_MISSING" json:"up_build_missing"`
	UpPrepare              bool     `mapstructure:"UP_PREPARE" json:"up_prepare"`
	BundleValidate         bool     `mapstructure:"BUNDLE_VALIDATE" json:"bundle_validate"`
	Bundle                 bool     `mapstructure:"BUNDLE" json:"bundle"`
	ForceBundle            bool     `mapstructure:"FORCE_BUNDLE" json:"force_bundle"`
	LabelsNamespace        string   `mapstructure:"LABELS_NAMESPACE" json:"labels_namespace"`

	LsServicesShort bool `mapstructure:"LS_SERVICES_SHORT" json:"ls_services_short"`

	ServiceDevTagEnabled bool
}

func (cfg *Config) AbsDeployBundlesDir() string {
	dir := cfg.DeployBundlesDir
	if len(dir) < 1 || dir[0:1] != "/" {
		pwd, _ := os.Getwd()
		dir = pwd + "/" + dir
	}
	return dir
}

func (cfg *Config) LoadCryptoKey(required bool) {

	key := cfg.CryptoKey

	if key == "" {
		if ok, _ := tools.FileExists(cfg.CryptoKeyFile); ok {
			b, err := ioutil.ReadFile(cfg.CryptoKeyFile)
			errors.Check(err)
			key = string(b)
		}
	}

	if key == "" && required {
		prompt := &survey.Password{
			Message: "crypto-key",
		}
		survey.AskOne(prompt, &key)
	}

	if key == "" && required {
		logrus.Fatal(FlagCryptoKeyMissingMsg)
	}

	cfg.CryptoKey = key

}

func (cfg *Config) GetDeployDir() string {
	deployDir := cfg.DeployDir
	if len(deployDir) > 0 && deployDir[0:1] != "/" {
		cwd, _ := os.Getwd()
		deployDir = cwd + "/" + deployDir
	}
	return deployDir
}
