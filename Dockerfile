ARG DOCKER_VERSION=19
ARG DOCKER_COMPOSE_VERSION=1.25.1-rc1
ARG GOMPLATE_VERSION=v3.6.0-slim
ARG GOLANG_VERSION=1

FROM docker:$DOCKER_VERSION AS docker
FROM docker/compose:$DOCKER_COMPOSE_VERSION AS docker-compose
FROM hairyhenderson/gomplate:$GOMPLATE_VERSION as gomplate
FROM golang:$GOLANG_VERSION as builder

RUN mkdir /opt/bin

# compile tyr
ENV GOFLAGS=-mod=vendor
WORKDIR /tyr
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o=/opt/bin/tyr .

# bash completion
RUN mkdir -p /etc/bash_completion.d && \
  printf "#!/bin/sh\n. <(tyr completion)">/etc/bash_completion.d/tyr && \
  chmod +x /etc/bash_completion.d/tyr

# create clean image
FROM scratch
COPY --from=docker          /usr/local/bin/docker           /usr/local/bin/docker
COPY --from=docker-compose  /usr/local/bin/docker-compose   /usr/local/bin/docker-compose
COPY --from=gomplate        /gomplate                       /usr/local/bin/gomplate
COPY --from=builder         /opt/bin/                       /usr/local/bin/
COPY --from=builder         /etc/bash_completion.d/tyr      /etc/bash_completion.d/tyr

ENTRYPOINT ["/usr/local/bin/tyr"]
CMD ["help"]