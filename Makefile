PROJECT_NAME := "tyr"
PKG := "gitlab.com/youtopia.earth/ops/$(PROJECT_NAME)"

all: vendor fmt build targz
install: install-bin autocomplete

docker:
	tyr build
dockerfile:
	docker build -t registry.gitlab.com/youtopia.earth/ops/tyr:$${TYR_TAG:-master} .
docker-compose:
	docker-compose build

vendor:
	go mod vendor

fmt:
	# gofmt -w .
	gofmt -w -l `find . -type f -name '*.go'| grep -v "/vendor/"`

build:
	CGO_ENABLED=0 GOOS=linux go build -o tyr -v $(PKG) .

targz:
	tar -cvzf $(PROJECT_NAME).tar.gz $(PROJECT_NAME)

install-bin:
	sudo cp -f tyr /usr/local/bin/tyr

autocomplete:
	[ -f ~/.bashrc ] || touch ~/.bashrc
	grep -xq ". <(tyr completion)" ~/.bashrc || printf "\n. <(tyr completion)\n" >> ~/.bashrc


help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
