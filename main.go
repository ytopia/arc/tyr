package main

import (
	"gitlab.com/youtopia.earth/ops/tyr/app"
)

func main() {
	app.New()
}
