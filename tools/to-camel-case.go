package tools

import (
	"regexp"
	"strings"
)

var camelCaseRe = regexp.MustCompile("(^[A-Za-z])|_([A-Za-z])")

func ToCamelCase(str string) string {
	return camelCaseRe.ReplaceAllStringFunc(str, func(s string) string {
		return strings.ToUpper(strings.Replace(s, "_", "", -1))
	})
}
