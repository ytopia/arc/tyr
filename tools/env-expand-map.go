package tools

import (
	"os"
	"strings"
)

func EnvExpandMap(envmap map[string]string, mergeMap map[string]string) {
	envmapper := func(key string) string {
		return envmap[key]
	}

	for k, v := range mergeMap {
		if k != "" {
			k = strings.ToUpper(k)
			envmap[k] = os.Expand(v, envmapper)
		}
	}
}
