package tools

import (
	"os"
	"strings"
)

func EnvExpandMapDefault(envmap map[string]string, mergeMap map[string]string, defaultMap map[string]string) {
	envmapper := func(key string) string {
		if value, hasKey := envmap[key]; hasKey {
			return value
		}
		return defaultMap[key]
	}

	for k, v := range mergeMap {
		if k != "" {
			k = strings.ToUpper(k)
			envmap[k] = os.Expand(v, envmapper)
		}
	}
}
