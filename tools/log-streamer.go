package tools

import (
	"log"

	"github.com/kvz/logstreamer"
	"github.com/sirupsen/logrus"
)

func LogStreamer() (*logstreamer.Logstreamer, func()) {
	fields := logrus.Fields{}
	return LogStreamerFields(fields)
}
func LogStreamerFields(fields logrus.Fields) (*logstreamer.Logstreamer, func()) {
	contextLogger := logrus.WithFields(fields)
	w := contextLogger.Writer()

	loggerOut := log.New(w, "", 0)
	logStreamer := logstreamer.NewLogstreamer(loggerOut, "", true)

	logStreamerClose := func() {
		w.Close()
		logStreamer.Close()
	}

	return logStreamer, logStreamerClose
}
