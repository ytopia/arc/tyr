package tools

import "strings"

func LinesToSlice(lines string) []string {
	var slice []string
	lines = strings.TrimRight(lines, "\n")
	if lines != "" {
		slice = strings.Split(lines, "\n")
	}
	return slice
}
