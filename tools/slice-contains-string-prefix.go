package tools

// SliceContainsStringPrefix tells whether a contains x prefix.
func SliceContainsStringPrefix(a []string, x string) bool {
	l := len(x)
	for _, n := range a {
		if x == n[0:l] {
			return true
		}
	}
	return false
}
