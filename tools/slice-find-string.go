package tools

// SliceFindString find returns the smallest index i at which x == a[i],
// or len(a) if there is no such index.
func SliceFindString(x string, a []string) int {
	for i, n := range a {
		if x == n {
			return i
		}
	}
	return len(a)
}
