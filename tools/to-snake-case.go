package tools

import (
	"regexp"
)

var snakeCaseReMatchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var snakeCaseReMatchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

func ToSnakeCase(str string) string {
	snake := snakeCaseReMatchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = snakeCaseReMatchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return snake
}
