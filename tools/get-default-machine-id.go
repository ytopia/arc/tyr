package tools

import (
	"io/ioutil"
	"strings"
)

func GetDefaultMachineId() string {
	idByte, _ := ioutil.ReadFile("/var/lib/dbus/machine-id")
	id := string(idByte)
	id = strings.TrimSpace(id)
	return id
}
