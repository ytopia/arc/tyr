module gitlab.com/youtopia.earth/ops/tyr

go 1.13

require (
	github.com/AlecAivazis/survey/v2 v2.0.5
	github.com/a8m/envsubst v1.1.0 // indirect
	github.com/docker/cli v0.0.0-20191212191748-ebca1413117a // indirect
	github.com/docker/docker v1.13.1 // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/drone/envsubst v1.0.2 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/google/go-jsonnet v0.14.0
	github.com/imdario/mergo v0.3.8 // indirect
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/json-iterator/go v1.1.8
	github.com/kvz/logstreamer v0.0.0-20150507115422-a635b98146f0
	github.com/mattn/go-shellwords v1.0.6 // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/opencontainers/runc v0.1.1 // indirect
	github.com/orcaman/concurrent-map v0.0.0-20190826125027-8c72a8bb44f6
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.5.0
	github.com/thanhpk/randstr v1.0.4
	github.com/xeipuuv/gojsonschema v1.2.0 // indirect
	github.com/yosuke-furukawa/json5 v0.1.1
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
	gopkg.in/yaml.v2 v2.2.4
)
