package compose

import "gitlab.com/youtopia.earth/ops/tyr/tools"

func StackGetServiceNames(app App, stack string) []string {
	var serviceNames []string
	composeFile := StackReadComposeFile(app, stack)

	for serviceName, _ := range composeFile.Services {
		if tools.SliceContainsString(serviceNames, serviceName) {
			continue
		}

		serviceNames = append(serviceNames, serviceName)
	}
	return serviceNames
}
