package compose

import (
	"io/ioutil"
	"os"
	"sync"

	cmap "github.com/orcaman/concurrent-map"
	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/environ"
	"gitlab.com/youtopia.earth/ops/tyr/goenv"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
	yaml "gopkg.in/yaml.v2"
)

type BuildOptions struct {
	App              App
	Name             string
	BuildMissingDeps bool
	BuildDeps        bool
	CommandArgs      []string
	Tasks            cmap.ConcurrentMap
	IsDependency     bool
	CleanEnv         bool
}

func Build(buildOptions *BuildOptions) {

	app := buildOptions.App
	buildName := buildOptions.Name
	tasks := buildOptions.Tasks

	cfg := app.GetConfig()
	rootDir := cfg.BuildsRootDir
	buildSrc := GetBuildConfig(app, buildName)

	var dir string
	if buildSrc.Dir != "" {
		dir = buildSrc.Dir
	} else {
		dir = buildName
	}
	if dir[0:1] != "/" {
		dir = rootDir + "/" + dir
	}

	var envMap map[string]string
	if buildOptions.CleanEnv {
		envMap = make(map[string]string)
	} else {
		envMap = tools.EnvToMap(os.Environ())
	}
	environ.BuildEnvMap(app, envMap, buildName)

	composeFilePath := dir + "/" + cfg.BuildComposeFile

	var err error
	var composeFileExists bool
	composeFileExists, err = tools.FileExists(composeFilePath)
	if err != nil {
		logrus.Fatal(err)
	}
	if !composeFileExists {
		if !buildOptions.BuildDeps && buildOptions.IsDependency {
			return
		}
		logrus.Fatalf("missing build compose file: %v", composeFilePath)
	}

	var composeFileContent []byte
	if bytes, err := ioutil.ReadFile(composeFilePath); err == nil {
		composeFileContent = bytes
	} else {
		logrus.Fatal(err)
	}

	var composeFile ComposeFile
	if err := yaml.Unmarshal(composeFileContent, &composeFile); err != nil {
		logrus.Fatal(err)
	}

	selectedServices := buildSrc.Services
	if len(selectedServices) == 0 {
		for service := range composeFile.Services {
			selectedServices = append(selectedServices, service)
		}
	}

	var buildImages []string

	if cfg.ServiceDevTagEnabled {
		PopulateServiceDevTagsToEnvMap(selectedServices, envMap, cfg.DevTag)
	}

	for _, service := range selectedServices {
		if serviceDef, hasKey := composeFile.Services[service]; !hasKey {
			logrus.Fatalf(`service "%v" not found on build "%v"`, service, buildName)
		} else {
			image := serviceDef["image"].(string)
			if str, err := goenv.Expand(image, envMap); err == nil {
				image = str
			} else {
				logrus.Fatal(err)
			}

			if image == "" {
				logrus.Fatalf(`image not found for service "%v" on build "%v"`, service, buildName)
			}

			if !buildOptions.IsDependency ||
				buildOptions.BuildDeps ||
				(buildOptions.BuildMissingDeps && !docker.ImageExists(image)) {
				buildImages = append(buildImages, image)
			}

		}
	}

	if buildOptions.BuildDeps || buildOptions.BuildMissingDeps {
		wg := &sync.WaitGroup{}

		for _, buildName := range buildSrc.DependsOn {
			wg.Add(1)
			go func(buildName string) {
				defer wg.Done()
				if tmp, ok := tasks.Get(buildName); ok {
					task := tmp.(*sync.WaitGroup)
					task.Wait()
				} else {
					task := &sync.WaitGroup{}
					tasks.Set(buildName, task)
					task.Add(1)
					buildOptionsDep := &BuildOptions{
						App:              buildOptions.App,
						Name:             buildName,
						CommandArgs:      buildOptions.CommandArgs,
						Tasks:            buildOptions.Tasks,
						CleanEnv:         buildOptions.CleanEnv,
						BuildMissingDeps: buildOptions.BuildMissingDeps,
						BuildDeps:        buildOptions.BuildDeps,
						IsDependency:     true,
					}
					Build(buildOptionsDep)
					task.Done()
				}
			}(buildName)
		}
		wg.Wait()
	}

	if cfg.ServiceDevTagEnabled {
		PopulateServiceDevVersionsToEnvMap(buildSrc.DependsOn, envMap, cfg.DevTag)
	}

	if len(buildImages) > 0 {
		logrus.Infof("building %v:", buildName)
		for _, buildImage := range buildImages {
			logrus.Infof("- %v", buildImage)
		}
		buildCommandOptions := &BuildCommandOptions{
			Name:     buildName,
			Dir:      dir,
			Args:     buildOptions.CommandArgs,
			Services: buildSrc.Services,
			Env:      envMap,
		}
		BuildCommand(buildCommandOptions)
	}
}
