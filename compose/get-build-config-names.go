package compose

import (
	"path/filepath"
	"strings"

	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func GetBuildConfigNames(app App) []string {
	cfg := app.GetConfig()

	var err error

	var buildConfigNames []string
	var files []string
	files, err = tools.OSReadDir(cfg.BuildsDir)
	for _, file := range files {
		ext := filepath.Ext(file)
		if ext == ".jsonnet" {
			buildConfigName := strings.TrimSuffix(filepath.Base(file), filepath.Ext(file))
			buildConfigNames = append(buildConfigNames, buildConfigName)
		}
	}
	errors.Check(err)

	return buildConfigNames
}
