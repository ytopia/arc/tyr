package compose

import (
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func StackGetBuildNames(app App, stack string) []string {
	var buildNames []string
	composeFile := StackReadComposeFile(app, stack)

	definedBuildNames := GetBuildConfigNames(app)

	for serviceName, service := range composeFile.Services {
		var buildName = serviceName
		if xTyrBuild, hasKey := service["x-tyr-build"]; hasKey {
			buildName = xTyrBuild.(string)
		}

		if !tools.SliceContainsString(definedBuildNames, buildName) {
			continue
		}
		if tools.SliceContainsString(buildNames, buildName) {
			continue
		}

		buildNames = append(buildNames, buildName)
	}
	return buildNames
}
