package compose

import (
	"bytes"
	"io/ioutil"
	"os"
	"os/exec"

	"github.com/sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

func ConfigUnmarshal(extends [][]byte, projectDir string) *ComposeFile {
	merged := &ComposeFile{}
	mergedBytes := Config(extends, projectDir)
	if err := yaml.Unmarshal(mergedBytes, merged); err != nil {
		logrus.Fatal(err)
	}
	return merged
}

func Config(extends [][]byte, projectDir string) []byte {

	if len(projectDir) > 0 && projectDir[0:1] != "/" {
		cwd, _ := os.Getwd()
		projectDir = cwd + "/" + projectDir
	}

	var composeFileNames []string
	for _, extend := range extends {

		tmpFile, err := ioutil.TempFile(os.TempDir(), "tyr-stack-")
		if err != nil {
			logrus.Fatal(err)
		}
		defer os.Remove(tmpFile.Name())

		if _, err = tmpFile.Write(extend); err != nil {
			logrus.Fatal(err)
		}

		if err := tmpFile.Close(); err != nil {
			logrus.Fatal(err)
		}

		composeFileNames = append(composeFileNames, tmpFile.Name())

	}

	var commandArgs []string
	for _, composeFileName := range composeFileNames {
		commandArgs = append(commandArgs, []string{"-f", composeFileName}...)
	}
	commandArgs = append(commandArgs, []string{"--project-directory", projectDir}...)

	commandArgs = append(commandArgs, []string{"config", "--no-interpolate"}...)

	var err error
	var mergedBytes []byte
	cmd := exec.Command("docker-compose", commandArgs...)
	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	mergedBytes, err = cmd.Output()
	if err != nil {
		logrus.Error(stderr.String())
		logrus.Fatal(err)
	}

	return mergedBytes
}
