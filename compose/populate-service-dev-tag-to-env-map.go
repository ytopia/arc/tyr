package compose

import "gitlab.com/youtopia.earth/ops/tyr/tools"

func PopulateServiceDevVersionsToEnvMap(services []string, envMap map[string]string, devTag string) {
	PopulateServiceTagsToEnvMap(services, envMap, devTag, "VERSION")
}

func PopulateServiceDevTagsToEnvMap(services []string, envMap map[string]string, devTag string) {
	PopulateServiceTagsToEnvMap(services, envMap, devTag, "TAG")
}

func PopulateServiceTagsToEnvMap(services []string, envMap map[string]string, devTag string, suffix string) {
	for _, service := range services {
		PopulateServiceTagToEnvMap(service, envMap, devTag, suffix)
	}
}

func PopulateServiceTagToEnvMap(service string, envMap map[string]string, devTag string, suffix string) {
	sKey := tools.KeyEnv(service)
	envKey := sKey + "_" + suffix
	envMap[envKey] = devTag
}
