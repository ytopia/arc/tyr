package compose

import (
	"path/filepath"
	"strings"

	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func GetStackNames(app App) []string {
	cfg := app.GetConfig()

	var err error

	var stackNames []string

	var files []string
	files, err = tools.OSReadDir(cfg.DeployDir)
	for _, file := range files {
		ext := filepath.Ext(file)
		if ext == ".jsonnet" {
			stack := strings.TrimSuffix(filepath.Base(file), filepath.Ext(file))
			stackNames = append(stackNames, stack)
		}
	}
	errors.Check(err)

	return stackNames
}
