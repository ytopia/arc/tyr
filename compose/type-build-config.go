package compose

// BuildConfig struct
type BuildConfig struct {
	DependsOn []string `yaml:"depends_on"`
	Dir       string   `yaml:"dir"`
	Services  []string `yaml:"services"`
}
