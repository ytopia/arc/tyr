package compose

import (
	"io/ioutil"

	"github.com/sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

func ReadComposeFile(composeFilePath string) *ComposeFile {
	var composeFileContent []byte
	if bytes, err := ioutil.ReadFile(composeFilePath); err == nil {
		composeFileContent = bytes
	} else {
		logrus.Fatal(err)
	}

	var composeFile = &ComposeFile{}
	if err := yaml.Unmarshal(composeFileContent, composeFile); err != nil {
		logrus.Fatal(err)
	}

	return composeFile
}
