package compose

import (
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func GetBuildNames(app App) []string {
	stackNames := GetStackNames(app)

	var allBuildNames []string
	for _, stack := range stackNames {
		buildNames := StackGetBuildNames(app, stack)
		for _, buildName := range buildNames {
			if !tools.SliceContainsString(allBuildNames, buildName) {
				allBuildNames = append(allBuildNames, buildName)
			}
		}
	}
	return allBuildNames
}
