package compose

import (
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

type BuildCommandOptions struct {
	Name     string
	Dir      string
	Args     []string
	Services []string
	Env      map[string]string
}

func BuildCommand(opts *BuildCommandOptions) {
	commandBin := "docker-compose"
	commandArgs := []string{"build"}
	commandArgs = append(commandArgs, opts.Args...)
	commandArgs = append(commandArgs, opts.Services...)
	logrus.Debugf("%v$ %v %v", opts.Dir, commandBin, strings.Join(commandArgs, " "))
	command := exec.Command(commandBin, commandArgs...)
	envPairs := tools.EnvToPairs(opts.Env)
	command.Env = envPairs

	logFields := logrus.Fields{
		"build": opts.Name,
	}
	logStreamer, logStreamerClose := tools.LogStreamerFields(logFields)
	defer logStreamerClose()
	command.Stdout = logStreamer
	command.Stderr = logStreamer

	command.Dir = opts.Dir
	if err := command.Run(); err != nil {
		logrus.Fatal(err)
	}
}
