package compose

import (
	"bytes"
	"os/exec"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func Validate(dir string, composeFilePath string, envMap map[string]string) error {
	cmd := exec.Command("docker-compose", "--project-directory", dir, "-f", composeFilePath, "config", "-q")
	cmd.Env = tools.EnvToPairs(envMap)
	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		logrus.Error(stderr.String())
	}
	return err
}
