package compose

func StackReadComposeFile(app App, stack string) *ComposeFile {
	cfg := app.GetConfig()
	composeFilePath := cfg.DeployBundlesDir + "/" + stack + ".yml"
	return ReadComposeFile(composeFilePath)
}
