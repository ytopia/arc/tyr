package compose

import (
	"bytes"
	"os/exec"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/environ"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func DeployStackContent(app App, stack string, stackFileContent []byte) {
	cfg := app.GetConfig()

	nsStack := config.PrefixNS(stack, cfg.StackNamespace)

	envMap := environ.StackEnvMapFromBundle(app, stack)
	commandArgs := []string{"stack", "deploy", "--compose-file", "-", nsStack}

	command := exec.Command("docker", commandArgs...)
	command.Env = tools.EnvToPairs(envMap)
	command.Dir = cfg.GetDeployDir()
	command.Stdin = bytes.NewReader(stackFileContent)

	logStreamer, logStreamerClose := tools.LogStreamerFields(logrus.Fields{
		"stack": stack,
	})
	defer logStreamerClose()
	command.Stdout = logStreamer
	command.Stderr = logStreamer

	err := command.Run()
	errors.Check(err)
}
