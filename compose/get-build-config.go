package compose

import (
	"gitlab.com/youtopia.earth/ops/tyr/environ"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/xjsonnet"
	yaml "gopkg.in/yaml.v2"
)

func GetBuildConfig(app App, buildName string) *BuildConfig {
	cfg := app.GetConfig()

	buildFilePath := cfg.BuildsDir + "/" + buildName + ".jsonnet"
	envMap := make(map[string]string)
	environ.BuildEnvMap(app, envMap, buildName)

	var err error

	var composeJSON string
	composeJSON, err = xjsonnet.RenderEnv(buildFilePath, envMap)

	errors.Check(err)

	buildConfig := &BuildConfig{}
	err = yaml.Unmarshal([]byte(composeJSON), buildConfig)
	errors.Check(err)

	return buildConfig
}
