package compose

import (
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/youtopia.earth/ops/tyr/environ"
	"gitlab.com/youtopia.earth/ops/tyr/goenv"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func StackRequiredImages(app App, stack string) []string {
	composeFile := StackReadComposeFile(app, stack)

	var images []string

	var envMap map[string]string
	envMap = tools.EnvToMap(os.Environ())
	environ.StackEnvMap(app, envMap, stack)
	StackEnvMapServiceDevTag(app, envMap, stack)

	for _, serviceDef := range composeFile.Services {
		image := serviceDef["image"].(string)
		if str, err := goenv.Expand(image, envMap); err == nil {
			image = str
		} else {
			logrus.Fatal(err)
		}
		if image != "" {
			images = append(images, image)
		}
	}

	return images

}
