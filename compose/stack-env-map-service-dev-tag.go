package compose

func StackEnvMapServiceDevTag(app App, envMap map[string]string, stack string) {
	cfg := app.GetConfig()
	composeFile := StackReadComposeFile(app, stack)
	services := make([]string, len(composeFile.Services))
	i := 0
	for k := range composeFile.Services {
		services[i] = k
		i++
	}
	PopulateServiceDevTagsToEnvMap(services, envMap, cfg.DevTag)
}
