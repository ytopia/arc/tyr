package compose

type ComposeFile struct {
	Services map[string]map[string]interface{} `mapstructure:"services" json:"services,omitempty"`
	Networks map[string]interface{}            `mapstructure:"networks" json:"networks,omitempty"`
	Volumes  map[string]interface{}            `mapstructure:"volumes"  json:"volumes,omitempty"`
	Configs  map[string]interface{}            `mapstructure:"configs" json:"configs,omitempty"`
	Secrets  map[string]interface{}            `mapstructure:"secrets"  json:"secrets,omitempty"`
	Version  string                            `mapstructure:"version"  json:"version,omitempty"`
}
