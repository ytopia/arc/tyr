package crypto

import (
	"fmt"

	"github.com/thanhpk/randstr"
	"gitlab.com/youtopia.earth/ops/tyr/reggen"
)

type RandomConfig struct {
	Regex  string
	Chars  string
	Size   int
	Prefix string
}

func Random(cfg RandomConfig) (string, error) {
	var val string
	if cfg.Regex != "" && cfg.Chars != "" {
		return val, fmt.Errorf("regex and chars options are mutually exclusive")
	}
	if cfg.Regex == "" {
		if cfg.Chars == "" {
			val = randstr.String(cfg.Size)
		} else {
			val = randstr.String(cfg.Size, cfg.Chars)
		}
	} else {
		var err error
		val, err = reggen.Generate(cfg.Regex, cfg.Size)
		return val, err
	}
	val = cfg.Prefix + val
	return val, nil
}
