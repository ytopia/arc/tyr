package environ

import (
	"strings"
)

func CommonEnvMap(app App, envMap map[string]string) {
	cfg := app.GetConfig()
	cl := app.GetConfigLoader()
	envMap[cl.PrefixEnv("ENV")] = strings.Join(cfg.Env, ",")
	envMap[cl.PrefixEnv("DEV_TAG")] = cfg.DevTag
	envMap[cl.PrefixEnv("MACHINE_ID")] = cfg.MachineID
	envMap[cl.PrefixEnv("CWD")] = cfg.CWD
}
