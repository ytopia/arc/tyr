package environ

func BuildEnvMap(app App, envMap map[string]string, buildName string) {
	cfg := app.GetConfig()
	envMapConfig := EnvMapConfig{
		EnvKey: "BUILD_ENV",
		Dir:    cfg.BuildsDir,
		Name:   buildName,
	}
	LoadEnvMap(app, envMap, envMapConfig)
}
