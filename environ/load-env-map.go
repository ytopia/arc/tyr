package environ

import (
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/goenv"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func LoadEnvMap(app App, envMap map[string]string, envMapConfig EnvMapConfig) {
	cl := app.GetConfigLoader()
	CommonEnvMap(app, envMap)

	envOrigin := tools.EnvToMap(os.Environ())

	pwd, _ := os.Getwd()
	envfile := pwd + "/" + envMapConfig.Dir + "/" + envMapConfig.Name + ".env"
	if ok, err := tools.FileExists(envfile); ok {
		goenv.ReadDefault(envfile, envMap, envOrigin)
	} else if err != nil {
		logrus.Fatal(err)
	}

	envKey := cl.PrefixEnv(envMapConfig.EnvKey)
	tyrEnv := envMap[envKey]

	if tyrEnv == "" {
		envKey = cl.PrefixEnv("ENV")
		tyrEnv = envMap[envKey]
	}

	envs := strings.Split(tyrEnv, ",")

	for _, envAdd := range envs {
		envfile := pwd + "/" + envMapConfig.Dir + "/" + envMapConfig.Name + ".env." + envAdd
		if ok, err := tools.FileExists(envfile); ok {
			goenv.ReadDefault(envfile, envMap, envOrigin)
		} else if err != nil {
			logrus.Fatal(err)
		}
	}
}
