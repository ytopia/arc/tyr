package environ

import (
	"gitlab.com/youtopia.earth/ops/tyr/config"
)

type App interface {
	GetConfig() *config.Config
	GetConfigLoader() *config.ConfigLoader
}
