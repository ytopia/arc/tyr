package environ

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/goenv"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func StackEnvMapFromBundle(app App, stack string) map[string]string {
	cfg := app.GetConfig()

	envfile := cfg.AbsDeployBundlesDir() + "/" + stack + ".env"
	var envMap = make(map[string]string)
	if ok, err := tools.FileExists(envfile); ok {
		goenv.Read(envfile, envMap)
	} else if err != nil {
		logrus.Fatal(err)
	}
	return envMap
}
