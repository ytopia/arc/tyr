package environ

func StackEnvMap(app App, envMap map[string]string, stack string) {
	SharedEnvMap(app, envMap, stack)
	cfg := app.GetConfig()
	envMapConfig := EnvMapConfig{
		EnvKey: "STACK_ENV",
		Dir:    cfg.DeployDir,
		Name:   stack,
	}
	LoadEnvMap(app, envMap, envMapConfig)
}
