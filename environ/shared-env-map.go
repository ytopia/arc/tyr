package environ

func SharedEnvMap(app App, envMap map[string]string, stack string) {
	cfg := app.GetConfig()
	envMapConfig := EnvMapConfig{
		EnvKey: "STACK_ENV",
		Dir:    cfg.SharedEnvDir,
		Name:   "",
	}
	LoadEnvMap(app, envMap, envMapConfig)
}
