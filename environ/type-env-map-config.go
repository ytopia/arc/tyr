package environ

type EnvMapConfig struct {
	EnvKey string
	Dir    string
	Name   string
}
