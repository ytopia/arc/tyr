package docker

import (
	"bytes"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

func CleanImmutableByPrefix(immutableType ImmutableType, immutableSelectedName string, keepLatest bool) {
	cleanImmutable(immutableType, immutableSelectedName, keepLatest, true)
}
func CleanImmutable(immutableType ImmutableType, immutableSelectedName string, keepLatest bool) {
	cleanImmutable(immutableType, immutableSelectedName, keepLatest, false)
}
func cleanImmutable(immutableType ImmutableType, immutableSelectedName string, keepLatest bool, byPrefix bool) {
	var immutableNamesParts [][]string

	var immutableCmd string
	var immutableTextLabel string
	switch immutableType {
	case ImmutableConfig:
		immutableNamesParts = ConfigNameParts(immutableSelectedName)
		immutableCmd = "config"
		immutableTextLabel = "config"
	case ImmutableSecret:
		immutableNamesParts = SecretNamesParts(immutableSelectedName)
		immutableCmd = "secret"
		immutableTextLabel = "secret"
	}

	var immutableLastCreatedMap = make(map[string]string)
	prefixL := len(immutableSelectedName)

	for _, immutableNameParts := range immutableNamesParts {
		immutableRootname := immutableNameParts[0]
		if immutableSelectedName == "" ||
			immutableSelectedName == immutableRootname ||
			(byPrefix && immutableRootname[0:prefixL] == immutableSelectedName) {
			immutableHash := immutableNameParts[1]
			immutableFullName := immutableRootname + "_" + immutableHash

			if keepLatest {
				if _, hasKey := immutableLastCreatedMap[immutableRootname]; !hasKey {
					immutableLastCreatedMap[immutableRootname] = FindImmutableLatestCreated(immutableType, immutableRootname)
				}
				if immutableLastCreatedMap[immutableRootname] == immutableFullName {
					continue
				}
			}

			command := exec.Command("docker", immutableCmd, "rm", immutableFullName)
			stderr := new(bytes.Buffer)
			command.Stderr = stderr
			if err := command.Run(); err != nil {
				if strings.Contains(stderr.String(), "is in use") {
					logrus.Infof("%v %v_%v is in use", immutableTextLabel, immutableRootname, immutableHash)
				} else {
					logrus.Fatal(err)
				}
			} else {
				logrus.Infof("%v %v_%v was removed", immutableTextLabel, immutableRootname, immutableHash)
			}
		}
	}

}
