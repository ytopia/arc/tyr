package docker

func SecretNamesHashMap(filter string, args ...string) map[string][]string {
	secretList := Secrets(filter, args...)
	hashMap := make(map[string][]string)
	for _, parts := range ImmutableNameParts(secretList) {
		name := parts[0]
		hash := parts[1]
		if _, hasKey := hashMap[name]; !hasKey {
			hashMap[name] = []string{}
		}
		hashMap[name] = append(hashMap[name], hash)
	}
	return hashMap
}
