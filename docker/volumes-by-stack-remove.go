package docker

import (
	"os/exec"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func VolumesByStackRemove(stack string) {
	wg := &sync.WaitGroup{}
	volumeList := VolumesByStack(stack)
	for _, volume := range volumeList {
		wg.Add(1)
		go func(volume string) {
			defer wg.Done()
			logrus.Debugf(`waiting while volume "%v" is in use`, volume)
			for {
				command := exec.Command("docker", "volume", "rm", "-f", volume)
				command.Run()
				volumeList := VolumesByStack(stack)
				foundVolume := tools.SliceContainsString(volumeList, volume)
				if foundVolume {
					time.Sleep(1 * time.Second)
				} else {
					break
				}
			}

			logrus.Infof(`volume "%v" was deleted`, volume)
		}(volume)
	}
	wg.Wait()
}
