package docker

import (
	"io/ioutil"

	"gitlab.com/youtopia.earth/ops/tyr/errors"
)

func HashImmutableFile(file string) string {
	bytes, err := ioutil.ReadFile(file)
	errors.Check(err)
	return HashImmutableBytes(bytes)
}
