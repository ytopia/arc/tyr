package docker

import (
	"crypto/sha1"
	"encoding/hex"
)

func HashImmutableBytes(bytes []byte) string {
	h := sha1.New()
	h.Write(bytes)
	sha1_hash := hex.EncodeToString(h.Sum(nil))
	return sha1_hash[0:HashLenght]
}
