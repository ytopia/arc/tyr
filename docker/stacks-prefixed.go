package docker

import (
	"os/exec"

	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func StacksPrefixed(namespace string) ([]string, error) {
	out, err := exec.Command("docker", "stack", "ls", "--format", "{{.Name}}").Output()
	if err != nil {
		return nil, err
	}
	allStacks := tools.ByteLinesToSlice(out)

	var stacks []string
	l := len(namespace) + 1
	for _, stack := range allStacks {
		if namespace == "" || (len(stack) > l && stack[0:l] == namespace+"_") {
			stacks = append(stacks, stack)
		}
	}

	return stacks, nil
}
