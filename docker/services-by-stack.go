package docker

import (
	"os/exec"

	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func ServicesByStack(stack string) ([]string, error) {
	return getServicesByStack(stack, false)
}

func FullServicesByStack(stack string) ([]string, error) {
	return getServicesByStack(stack, true)
}

func getServicesByStack(stack string, fullNames bool) ([]string, error) {
	var services []string

	out, err := exec.Command("docker", "stack", "services", "--format", "{{.Name}}", stack).Output()
	if err != nil {
		return nil, err
	}

	serviceFullnames := tools.ByteLinesToSlice(out)

	if fullNames {
		return serviceFullnames, nil
	}

	stackLen := len(stack) + 1
	for _, serviceFullname := range serviceFullnames {
		services = append(services, serviceFullname[stackLen:])
	}
	return services, nil
}
