package docker

import (
	"os/exec"

	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func Services() ([]string, error) {
	out, err := exec.Command("docker", "service", "ls", "--format", "{{.Name}}").Output()
	if err != nil {
		return nil, err
	}
	services := tools.ByteLinesToSlice(out)
	return services, nil
}
