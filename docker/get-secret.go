package docker

import (
	"crypto/sha1"
	"encoding/hex"
	"os/exec"
	"strings"

	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func GetSecret(secretName string) string {

	h := sha1.New()
	h.Write([]byte(secretName))
	hash := hex.EncodeToString(h.Sum(nil))
	serviceName := "tyrtmp_" + hash

	serviceNames, err := Services()
	errors.Check(err)

	if !tools.SliceContainsString(serviceNames, serviceName) {
		cmd := exec.Command("docker", "service", "create", "--secret", secretName, "--name", serviceName, "busybox", "tail", "-f")
		err := cmd.Run()
		errors.Check(err)
	}

	out, err := exec.Command("docker", "container", "ls", "-q", "--filter", `label=com.docker.swarm.service.name=`+serviceName).Output()
	errors.Check(err)
	containerList := tools.ByteLinesToSlice(out)
	containerName := containerList[0]

	out, err = exec.Command("docker", "exec", containerName, "cat", "/run/secrets/"+secretName).Output()
	errors.Check(err)
	secretValue := strings.TrimRight(string(out), "\n")

	cmd := exec.Command("docker", "service", "rm", serviceName)
	err = cmd.Run()
	errors.Check(err)

	return secretValue
}
