package docker

import (
	"os/exec"

	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func ServicesPrefixed(namespace string) ([]string, error) {
	out, err := exec.Command("docker", "service", "ls", "--format", "{{.Name}}").Output()
	if err != nil {
		return nil, err
	}
	allServices := tools.ByteLinesToSlice(out)

	var services []string
	l := len(namespace) + 1
	for _, service := range allServices {
		if namespace == "" || (len(service) > l && service[0:l] == namespace+"_") {
			services = append(services, service)
		}
	}

	return services, nil
}
