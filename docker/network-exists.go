package docker

import (
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

func NetworkExists(network string) bool {
	out, err := exec.Command("docker", "network", "ls", "-q", "--filter", "name="+network).Output()
	if err != nil {
		logrus.Fatal(err)
	}
	raw := strings.TrimRight(string(out), "\n")
	if raw != "" {
		return true
	}
	return false
}
