package docker

import (
	"os/exec"
	"strings"
	"time"

	"gitlab.com/youtopia.earth/ops/tyr/errors"
)

func SecretCreatedTime(secret string) time.Time {
	var createdDate time.Time
	out, err := exec.Command("docker", "secret", "inspect", "--format", "{{.CreatedAt}}", secret).Output()
	raw := strings.TrimRight(string(out), "\n")
	if raw != "" {
		createdDate, err = time.Parse("2006-01-02 15:04:05 -0700 MST", raw)
		errors.Check(err)
	}
	return createdDate
}
