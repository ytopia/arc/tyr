package docker

import (
	"os/exec"

	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func ContainersByService(service string) ([]string, error) {
	return getContainersByService(service, false)
}

func FullContainersByService(service string) ([]string, error) {
	return getContainersByService(service, true)
}

func getContainersByService(service string, fullNames bool) ([]string, error) {
	out, err := exec.Command("docker", "container", "ls", "--format", "{{.Names}}", "--filter", "label=com.docker.swarm.service.name="+service).Output()
	if err != nil {
		return nil, err
	}
	containerFullnames := tools.ByteLinesToSlice(out)

	if fullNames {
		return containerFullnames, nil
	}

	serviceLen := len(service) + 1
	var containers []string
	for _, containerFullname := range containerFullnames {
		containers = append(containers, containerFullname[serviceLen:])
	}
	return containers, nil
}
