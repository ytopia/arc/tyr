package docker

func ConfigNameParts(filter string, args ...string) [][]string {
	configList := Configs(filter, args...)
	return ImmutableNameParts(configList)
}
