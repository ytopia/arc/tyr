package docker

import (
	"os/exec"
	"sort"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

type ImmutableInfo struct {
	CreatedDate time.Time
	FullName    string
}

func FindImmutablePreviousCreated(immutableType ImmutableType, immutableName string) string {
	var immutableCmd string
	switch immutableType {
	case ImmutableConfig:
		immutableCmd = "config"
	case ImmutableSecret:
		immutableCmd = "secret"
	}

	var err error
	var out []byte
	var immutableFullNames []string

	out, err = exec.Command("docker", immutableCmd, "ls", "-q", "--format", "{{.Name}}", "--filter", `name=`+immutableName).Output()
	if err != nil {
		logrus.Fatal(err)
	}
	immutableFullNames = tools.ByteLinesToSlice(out)

	var immutableSlice []ImmutableInfo
	for _, immutableFullName := range immutableFullNames {
		if !ImmutableIsHashedName(immutableFullName) || immutableFullName[0:len(immutableFullName)-HashLenght-1] != immutableName {
			continue
		}
		out, err = exec.Command("docker", immutableCmd, "inspect", "--format", "{{.CreatedAt}}", immutableFullName).Output()
		raw := strings.TrimRight(string(out), "\n")
		if raw == "" {
			continue
		}

		var createdDate time.Time
		createdDate, err = time.Parse("2006-01-02 15:04:05 -0700 MST", raw)
		if err != nil {
			logrus.Fatal(err)
		}

		immutableSlice = append(immutableSlice, ImmutableInfo{
			CreatedDate: createdDate,
			FullName:    immutableFullName,
		})

	}

	sort.SliceStable(immutableSlice, func(i, j int) bool {
		return immutableSlice[i].CreatedDate.After(immutableSlice[j].CreatedDate)
	})

	var immutableFullName string
	if len(immutableSlice) > 1 {
		immutableFullName = immutableSlice[1].FullName
	}
	return immutableFullName

}
