package docker

import (
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

func ImageExists(image string) bool {
	out, err := exec.Command("docker", "images", "-q", image).Output()
	if err != nil {
		logrus.Fatal(err)
	}
	raw := strings.TrimRight(string(out), "\n")
	if raw != "" {
		return true
	}
	return false
}
