package docker

type ImmutableType int

const (
	ImmutableSecret ImmutableType = iota
	ImmutableConfig
)
