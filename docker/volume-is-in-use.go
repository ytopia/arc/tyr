package docker

import (
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

func VolumeIsInUse(volume string) bool {
	if out, err := exec.Command("docker", "ps", "-q", "-f", "volume="+volume).Output(); err != nil {
		logrus.Fatal(err)
	} else {
		raw := strings.TrimRight(string(out), "\n")
		if raw == "" {
			return false
		}
	}
	return true
}
