package docker

import (
	"regexp"
	"strconv"
)

const HashLenght = 26

var hashLenStr = strconv.Itoa(HashLenght)
var reConfigHashedName = regexp.MustCompile(`.*_[0-9a-fA-F]{` + hashLenStr + `}$`)

func ImmutableIsHashedName(configFullName string) bool {
	return reConfigHashedName.Match([]byte(configFullName))
}
