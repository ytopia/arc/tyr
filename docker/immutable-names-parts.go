package docker

func ImmutableNameParts(iList []string) [][]string {
	var iNameParts [][]string
	for _, iFullName := range iList {
		if ImmutableIsHashedName(iFullName) {
			iRootname := iFullName[0 : len(iFullName)-HashLenght-1]
			iHash := iFullName[len(iFullName)-HashLenght:]
			iNamePart := []string{iRootname, iHash}
			iNameParts = append(iNameParts, iNamePart)
		}
	}
	return iNameParts
}
