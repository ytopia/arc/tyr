package docker

import (
	"os/exec"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func Configs(filter string, args ...string) []string {
	var configList []string
	commandArgs := []string{"config", "ls", "-q", "--format", "{{.Name}}", "--filter=name=" + filter}
	commandArgs = append(commandArgs, args...)
	if out, err := exec.Command("docker", commandArgs...).Output(); err != nil {
		logrus.Fatal(err)
	} else {
		configList = tools.ByteLinesToSlice(out)
	}
	return configList
}
