package docker

import (
	"os/exec"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func Secrets(filter string, args ...string) []string {
	var secretList []string
	commandArgs := []string{"secret", "ls", "-q", "--format", "{{.Name}}", "--filter=name=" + filter}
	commandArgs = append(commandArgs, args...)
	if out, err := exec.Command("docker", commandArgs...).Output(); err != nil {
		logrus.Fatal(err)
	} else {
		secretList = tools.ByteLinesToSlice(out)
	}
	return secretList
}
