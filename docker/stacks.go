package docker

import (
	"os/exec"

	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func Stacks() ([]string, error) {
	out, err := exec.Command("docker", "stack", "ls", "--format", "{{.Name}}").Output()
	if err != nil {
		return nil, err
	}
	stacks := tools.ByteLinesToSlice(out)
	return stacks, nil
}
