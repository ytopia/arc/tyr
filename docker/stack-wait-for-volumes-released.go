package docker

import (
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

func StackWaitForVolumesReleased(stack string) {
	volumeList := VolumesByStack(stack)
	wg := &sync.WaitGroup{}
	for _, volume := range volumeList {
		wg.Add(1)
		go func(volume string) {
			defer wg.Done()
			var firstIteration = true
			for {
				if VolumeIsInUse(volume) {
					if firstIteration {
						logrus.Debugf(`waiting while volume "%v" is in use`, volume)
					}
					time.Sleep(1 * time.Second)
				} else {
					break
				}
				firstIteration = false
			}
			logrus.Debugf(`volume "%v" is released`, volume)
		}(volume)
	}
	wg.Wait()
}
