package docker

import (
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func PullImage(image string, force bool) {

	if !force {
		if out, err := exec.Command("docker", "image", "ls", "-q", "--filter", "reference="+image).Output(); err != nil {
			logrus.Fatal(err)
		} else {
			raw := strings.TrimRight(string(out), "\n")
			if raw != "" {
				return
			}
		}
	}

	cmd := exec.Command("docker", "pull", image)

	logStreamer, logStreamerClose := tools.LogStreamer()
	defer logStreamerClose()
	cmd.Stdout = logStreamer
	cmd.Stderr = logStreamer

	if err := cmd.Run(); err != nil {
		logrus.Fatal(err)
	}

}
