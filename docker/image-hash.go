package docker

import (
	"os/exec"
	"strings"
)

func ImageHash(image string) (string, error) {
	out, err := exec.Command("docker", "inspect", "--type", "image", "--format", "{{.Id}}", image).Output()
	if err != nil {
		return "", err
	}
	raw := strings.TrimRight(string(out), "\n")
	return raw, nil
}
