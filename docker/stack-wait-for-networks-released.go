package docker

import (
	"os/exec"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func StackWaitForNetworksReleased(stack string) {
	var firstIteration = true
	for {
		if out, err := exec.Command("docker", "network", "ls", "-q", "--format", "{{.Name}}", "--filter", "label=com.docker.stack.namespace="+stack).Output(); err != nil {
			logrus.Fatal(err)
		} else {
			networkList := tools.ByteLinesToSlice(out)
			if len(networkList) == 0 {
				if !firstIteration {
					logrus.Debugf("networks of stack %v are released", stack)
				}
				break
			}
			if firstIteration {
				logrus.Debugf("waiting while following networks are in use: %v", networkList)
			}
			time.Sleep(1 * time.Second)
		}
		firstIteration = false
	}
}
