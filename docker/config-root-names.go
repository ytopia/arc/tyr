package docker

func ConfigRootNames(filter string, args ...string) []string {
	configList := Configs(filter, args...)
	var configNames []string
	for _, configFullName := range configList {
		if ImmutableIsHashedName(configFullName) {
			configRootname := configFullName[0 : len(configFullName)-HashLenght-1]
			configNames = append(configNames, configRootname)
		}
	}
	return configNames
}
