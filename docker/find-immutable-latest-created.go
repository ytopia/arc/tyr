package docker

import (
	"os/exec"
	"strings"
	"time"

	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func FindImmutableLatestCreated(immutableType ImmutableType, immutableName string) string {
	var immutableCmd string
	switch immutableType {
	case ImmutableConfig:
		immutableCmd = "config"
	case ImmutableSecret:
		immutableCmd = "secret"
	}

	var err error
	var out []byte
	var immutableFullNames []string

	out, err = exec.Command("docker", immutableCmd, "ls", "-q", "--format", "{{.Name}}", "--filter", `name=`+immutableName).Output()
	errors.Check(err)
	immutableFullNames = tools.ByteLinesToSlice(out)

	var latestCreatedImmutable string
	var latestCreatedDate time.Time
	var createdDate time.Time
	for _, immutableFullName := range immutableFullNames {
		if !ImmutableIsHashedName(immutableFullName) || immutableFullName[0:len(immutableFullName)-HashLenght-1] != immutableName {
			continue
		}
		out, err = exec.Command("docker", immutableCmd, "inspect", "--format", "{{.CreatedAt}}", immutableFullName).Output()
		raw := strings.TrimRight(string(out), "\n")
		if raw == "" {
			continue
		}

		createdDate, err = time.Parse("2006-01-02 15:04:05 -0700 MST", raw)
		errors.Check(err)

		if latestCreatedImmutable == "" || createdDate.After(latestCreatedDate) {
			latestCreatedImmutable = immutableFullName
			latestCreatedDate = createdDate
		}

	}

	return latestCreatedImmutable

}
