package docker

import (
	"os/exec"
	"strings"

	"gitlab.com/youtopia.earth/ops/tyr/errors"
)

func SecretLabel(secret, label string) string {
	out, err := exec.Command("docker", "secret", "inspect", "--format", `{{index .Spec.Labels "`+label+`"}}`, secret).Output()
	errors.Check(err)
	return strings.TrimRight(string(out), "\n")
}
