package docker

import "gitlab.com/youtopia.earth/ops/tyr/tools"

func SecretRootNames(filter string, args ...string) []string {
	configList := Secrets(filter, args...)
	var configNames []string
	for _, configFullName := range configList {
		if ImmutableIsHashedName(configFullName) {
			configRootname := configFullName[0 : len(configFullName)-HashLenght-1]
			if !tools.SliceContainsString(configNames, configRootname) {
				configNames = append(configNames, configRootname)
			}
		}
	}
	return configNames
}
