package docker

import "sync"

func PullImages(images []string, force bool) {

	wg := &sync.WaitGroup{}
	for _, image := range images {
		wg.Add(1)
		go func(image string) {
			defer wg.Done()
			PullImage(image, force)
		}(image)
	}
	wg.Wait()

}
