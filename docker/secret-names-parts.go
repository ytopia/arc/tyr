package docker

func SecretNamesParts(filter string, args ...string) [][]string {
	secretList := Secrets(filter, args...)
	return ImmutableNameParts(secretList)
}
