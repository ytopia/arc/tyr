package docker

import "sync"

func StackWaitForRessourcesReleased(stack string) {
	wg := &sync.WaitGroup{}

	wg.Add(1)
	go func(stack string) {
		defer wg.Done()
		StackWaitForServicesReleased(stack)
	}(stack)

	wg.Add(1)
	go func(stack string) {
		defer wg.Done()
		StackWaitForNetworksReleased(stack)
	}(stack)

	wg.Add(1)
	go func(stack string) {
		defer wg.Done()
		StackWaitForVolumesReleased(stack)
	}(stack)

	wg.Wait()
}
