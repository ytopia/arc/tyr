package docker

import (
	"os/exec"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func VolumesByStack(stack string) []string {
	var volumeList []string
	if out, err := exec.Command("docker", "volume", "ls", "-q", "--filter", `label=com.docker.stack.namespace=`+stack).Output(); err != nil {
		logrus.Fatal(err)
	} else {
		volumeList = tools.ByteLinesToSlice(out)
	}
	return volumeList
}
