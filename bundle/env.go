package bundle

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/environ"
	"gitlab.com/youtopia.earth/ops/tyr/goenv"
)

func EnvInit(app App, stack string) map[string]string {
	var envMap = make(map[string]string)
	environ.StackEnvMap(app, envMap, stack)
	return envMap
}

func EnvWrite(app App, stack string, envMap map[string]string) error {
	cfg := app.GetConfig()
	bundleDir := cfg.DeployBundlesDir
	envFileTarget := bundleDir + "/" + stack + ".env"
	err := goenv.Write(envMap, envFileTarget)
	if err == nil {
		logrus.Debugf("bundle env file writed to %v", envFileTarget)
	}
	return err
}
