package bundle

import (
	"io/ioutil"

	"gitlab.com/youtopia.earth/ops/tyr/compose"
)

func WriteComposeFile(composeFile *compose.ComposeFile, stackFileTarget string) error {
	composeYAML, err := ComposeFileToYAML(composeFile)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(stackFileTarget, composeYAML, 0644)
	return err
}
