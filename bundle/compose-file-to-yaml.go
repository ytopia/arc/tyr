package bundle

import (
	"github.com/ghodss/yaml"
	json "github.com/json-iterator/go"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
)

func ComposeFileToYAML(composeFile *compose.ComposeFile) ([]byte, error) {
	var composeBytes []byte
	var err error
	composeBytes, err = json.Marshal(composeFile)
	if err != nil {
		return nil, err
	}

	var composeYAML []byte
	composeYAML, err = yaml.JSONToYAML(composeBytes)
	if err != nil {
		return nil, err
	}

	return composeYAML, nil
}
