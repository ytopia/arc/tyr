package bundle

import (
	"crypto/sha1"
	"encoding/hex"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/ghodss/yaml"
	"github.com/jinzhu/copier"
	json "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/crypto"
	"gitlab.com/youtopia.earth/ops/tyr/decode"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/goenv"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
	"gitlab.com/youtopia.earth/ops/tyr/xjsonnet"
)

func Compose(app App, stack string, envMap map[string]string, shared *compose.ComposeFile) (*compose.ComposeFile, error) {
	cfg := app.GetConfig()

	stackFileSrc := cfg.DeployDir + "/" + stack + ".jsonnet"
	stackFileTarget := cfg.DeployBundlesDir + "/" + stack + ".yml"

	var err error
	var composeFile = &compose.ComposeFile{}

	var composeJSON string
	composeJSON, err = xjsonnet.RenderEnv(stackFileSrc, envMap)
	if err != nil {
		return composeFile, err
	}

	if err := yaml.Unmarshal([]byte(composeJSON), composeFile); err != nil {
		return composeFile, err
	}

	composeFile = prefixWithNamespaces(app, composeFile)
	shared = prefixWithNamespaces(app, shared)

	disableServiceWithEnableFalse(composeFile)

	injectRequiredFromShared(composeFile, shared)
	injectImmutableNames(composeFile, envMap, app)
	injectDefaultComposeVersion(composeFile, cfg.ComposeVersion)

	if cfg.ServiceDevTagEnabled {
		populateServiceDevTagsToEnvMap(composeFile, envMap, cfg.DevTag)
	}
	if cfg.BundleUpdateImage {
		injectLabelUpdateImage(app, composeFile, envMap)
	}

	injectLabelSecrets(app, composeFile, envMap)

	if cfg.BundleUpdateStack {
		injectLabelUpdateStack(app, composeFile, envMap)
	}

	if err = WriteComposeFile(composeFile, stackFileTarget); err != nil {
		return composeFile, err
	}
	logrus.Debugf("bundle compose file writed to %v", stackFileTarget)

	return composeFile, err
}

func prefixWithNamespaces(app App, composeFile *compose.ComposeFile) *compose.ComposeFile {

	cfg := app.GetConfig()

	prefixedComposeFile := &compose.ComposeFile{}
	copier.Copy(prefixedComposeFile, composeFile)

	prefixedComposeFile.Networks = make(map[string]interface{})
	for k, v := range composeFile.Networks {
		prefixedComposeFile.Networks[config.PrefixNS(k, cfg.NetworkNamespace)] = v
	}

	prefixedComposeFile.Volumes = make(map[string]interface{})
	for k, v := range composeFile.Volumes {
		prefixedComposeFile.Volumes[config.PrefixNS(k, cfg.VolumeNamespace)] = v
		switch v.(type) {
		case map[string]interface{}:
			m := v.(map[string]interface{})
			if name, hasKeyName := m["name"]; hasKeyName {
				m["name"] = config.PrefixNS(name.(string), cfg.VolumeNamespace)
			}
		}
	}

	prefixedComposeFile.Secrets = make(map[string]interface{})
	for k, v := range composeFile.Secrets {
		prefixedComposeFile.Secrets[config.PrefixNS(k, cfg.SecretNamespace)] = v
		switch v.(type) {
		case map[string]interface{}:
			m := v.(map[string]interface{})
			if name, hasKeyName := m["name"]; hasKeyName {
				m["name"] = config.PrefixNS(name.(string), cfg.SecretNamespace)
			}
		}
	}

	prefixedComposeFile.Configs = make(map[string]interface{})
	for k, v := range composeFile.Configs {
		prefixedComposeFile.Configs[config.PrefixNS(k, cfg.ConfigNamespace)] = v
		switch v.(type) {
		case map[string]interface{}:
			m := v.(map[string]interface{})
			if name, hasKeyName := m["name"]; hasKeyName {
				m["name"] = config.PrefixNS(name.(string), cfg.ConfigNamespace)
			}
		}
	}

	for _, service := range prefixedComposeFile.Services {
		if service["networks"] != nil {
			networks := make(map[string]interface{})
			switch service["networks"].(type) {
			case map[string]interface{}:
				for networkName, v := range service["networks"].(map[string]interface{}) {
					networks[config.PrefixNS(networkName, cfg.NetworkNamespace)] = v
				}
			case []interface{}:
				for _, networkName := range service["networks"].([]interface{}) {
					networks[config.PrefixNS(networkName.(string), cfg.NetworkNamespace)] = make(map[string]interface{})
				}
			}
			service["networks"] = networks
		}

		if service["volumes"] != nil {
			volumes := service["volumes"].([]interface{})
			service["volumes"] = make([]interface{}, len(volumes))
			for i, volume := range volumes {
				switch volume.(type) {
				case map[string]interface{}:
					volumeDef := volume.(map[string]interface{})
					source := volumeDef["source"].(string)
					if source[0:1] != "$" &&
						source[0:1] != "/" &&
						source[0:2] != "./" &&
						source[0:3] != "../" {
						volumeDef["source"] = config.PrefixNS(source, cfg.VolumeNamespace)
					}
					service["volumes"].([]interface{})[i] = volumeDef
				case string:
					source := volume.(string)
					if source[0:1] != "$" &&
						source[0:1] != "/" &&
						source[0:2] != "./" &&
						source[0:3] != "../" {
						service["volumes"].([]interface{})[i] = config.PrefixNS(source, cfg.VolumeNamespace)
					} else {
						service["volumes"].([]interface{})[i] = source
					}
				}
			}
		}

		if service["secrets"] != nil {
			secrets := service["secrets"].([]interface{})
			service["secrets"] = make([]interface{}, len(secrets))
			for i, secret := range secrets {
				switch secret.(type) {
				case map[string]interface{}:
					secretDef := secret.(map[string]interface{})
					secretDef["source"] = config.PrefixNS(secretDef["source"].(string), cfg.SecretNamespace)
					service["secrets"].([]interface{})[i] = secretDef
				case string:
					service["secrets"].([]interface{})[i] = config.PrefixNS(secret.(string), cfg.SecretNamespace)
				}
			}
		}
		if service["configs"] != nil {
			configs := service["configs"].([]interface{})
			service["configs"] = make([]interface{}, len(configs))
			for i, conf := range configs {
				switch conf.(type) {
				case map[string]interface{}:
					configDef := conf.(map[string]interface{})
					configDef["source"] = config.PrefixNS(configDef["source"].(string), cfg.ConfigNamespace)
					service["configs"].([]interface{})[i] = configDef
				case string:
					service["configs"].([]interface{})[i] = config.PrefixNS(conf.(string), cfg.ConfigNamespace)
				}
			}
		}
	}

	return prefixedComposeFile
}

func disableServiceWithEnableFalse(composeFile *compose.ComposeFile) {
	for k, service := range composeFile.Services {
		xTyrEnable, hasKeyXTyrEnable := service["x-tyr-enable"]
		if hasKeyXTyrEnable && !xTyrEnable.(bool) {
			delete(composeFile.Services, k)
		}
	}
}

func injectDefaultComposeVersion(composeFile *compose.ComposeFile, defaultVersion string) {
	if composeFile.Version == "" {
		composeFile.Version = defaultVersion
	}
}

func injectRequiredFromShared(composeFile *compose.ComposeFile, shared *compose.ComposeFile) {

	serviceOptionsSliceOrMapTypes := []string{"networks"}
	serviceOptionsSliceTypes := []string{"volumes", "secrets", "config"}
	for _, service := range composeFile.Services {

		for _, optionType := range serviceOptionsSliceOrMapTypes {
			if _, ok := service[optionType]; ok {
				items := service[optionType]
				var itemNames []string
				switch items.(type) {
				case map[string]interface{}:
					for itemName, _ := range items.(map[string]interface{}) {
						itemNames = append(itemNames, itemName)
					}
				case []interface{}:
					for _, item := range items.([]interface{}) {
						itemName := item.(string)
						itemNames = append(itemNames, itemName)
					}
				}
				for _, itemName := range itemNames {
					requiredServiceOption(composeFile, shared, optionType, itemName)
				}
			}
		}

		for _, optionType := range serviceOptionsSliceTypes {
			if _, ok := service[optionType]; ok {
				items := service[optionType].([]interface{})
				var itemSource string
				for _, item := range items {
					switch item.(type) {
					case string:
						itemSource = item.(string)
					case map[string]interface{}:
						itemSource = (item.(map[string]interface{}))["source"].(string)
					}
					requiredServiceOption(composeFile, shared, optionType, itemSource)
				}
			}
		}

	}
}

func getServiceOptionByType(composeFile *compose.ComposeFile, optionType string) (option map[string]interface{}) {
	switch optionType {
	case "networks":
		option = composeFile.Networks
	case "volumes":
		option = composeFile.Volumes
	case "secrets":
		option = composeFile.Secrets
	case "configs":
		option = composeFile.Configs
	}
	return option
}

func setServiceOptionByType(composeFile *compose.ComposeFile, optionType string, optionName string, optionValue interface{}) (option map[string]interface{}) {
	switch optionType {
	case "networks":
		if composeFile.Networks == nil {
			composeFile.Networks = make(map[string]interface{})
		}
		option = composeFile.Networks
	case "volumes":
		if composeFile.Volumes == nil {
			composeFile.Volumes = make(map[string]interface{})
		}
		option = composeFile.Volumes
	case "secrets":
		if composeFile.Secrets == nil {
			composeFile.Secrets = make(map[string]interface{})
		}
		option = composeFile.Secrets
	case "configs":
		if composeFile.Configs == nil {
			composeFile.Configs = make(map[string]interface{})
		}
		option = composeFile.Configs
	}
	option[optionName] = optionValue
	return option
}

func requiredServiceOption(composeFile *compose.ComposeFile, shared *compose.ComposeFile, optionType string, optionName string) {
	composeOption := getServiceOptionByType(composeFile, optionType)
	if _, hasKey := composeOption[optionName]; hasKey {
		return
	}
	sharedOption := getServiceOptionByType(shared, optionType)
	if _, hasKey := sharedOption[optionName]; !hasKey {
		return
	}

	setServiceOptionByType(composeFile, optionType, optionName, sharedOption[optionName])
}

func injectImmutableNames(composeFile *compose.ComposeFile, envMap map[string]string, app App) {
	injectImmutableNameLoop(composeFile.Secrets, docker.ImmutableSecret, envMap, app)
	injectImmutableNameLoop(composeFile.Configs, docker.ImmutableConfig, envMap, app)
}

func injectImmutableNameLoop(composeSection map[string]interface{}, immutableType docker.ImmutableType, envMap map[string]string, app App) {
	if composeSection == nil {
		return
	}
	keys := make([]string, len(composeSection))
	i := 0
	for immutableName := range composeSection {
		keys[i] = immutableName
		i++
	}
	for _, immutableName := range keys {
		immutableOptions := composeSection[immutableName]
		composeSection[immutableName] = injectImmutableName(app, immutableType, composeSection, envMap, immutableName, immutableOptions)
	}
}

type ImmutableNameType int

const (
	ImmutableNameLast ImmutableNameType = iota
	ImmutableNamePrev
)

func injectImmutableName(app App, immutableType docker.ImmutableType, composeSection map[string]interface{}, envMap map[string]string, immutableName string, immutableOptionsInterface interface{}) map[string]interface{} {

	var immutableOptions map[string]interface{}
	var err error
	immutableOptions, err = decode.ToMap(immutableOptionsInterface)
	errors.Check(err)

	var immutablePrefix string
	switch immutableType {
	case docker.ImmutableConfig:
		immutablePrefix = "CONFIG"
	case docker.ImmutableSecret:
		immutablePrefix = "SECRET"
	}

	cl := app.GetConfigLoader()

	xTyrRandom, hasKeyXTyrRandom := immutableOptions["x-tyr-random"]
	if hasKeyXTyrRandom && xTyrRandom.(bool) {
		ensureImmutableRandomExists(app, immutableType, immutableName, immutableOptions)
	}

	xTyrName, hasKeyXTyrName := immutableOptions["x-tyr-env-name-use"]
	if !hasKeyXTyrName {
		return immutableOptions
	}

	var immutableNameType ImmutableNameType
	switch xTyrName.(type) {
	case bool:
		if !xTyrName.(bool) {
			return immutableOptions
		}
		immutableNameType = ImmutableNameLast
	case string:
		switch xTyrName.(string) {
		case "last":
			immutableNameType = ImmutableNameLast
		case "prev":
			immutableNameType = ImmutableNamePrev
		default:
			logrus.Fatalf("unexpected x-tyr-env-name-use value %v", xTyrName)
		}
	default:
		logrus.Fatalf("unexpected x-tyr-env-name-use value %v", xTyrName)
	}

	external, hasKeyExternal := immutableOptions["external"]
	_, hasKeyFile := immutableOptions["file"]
	_, hasKeyEncryptedFile := immutableOptions["x-tyr-encrypted-file"]

	if !((hasKeyExternal && external.(bool)) || hasKeyFile || hasKeyEncryptedFile) {
		return immutableOptions
	}

	if hasKeyFile && hasKeyEncryptedFile {
		logrus.Fatal("immutable (secret or config) x-tyr-encrypted-file and file keys are mutually exclusive")
	}

	cfg := app.GetConfig()

	if hasKeyEncryptedFile && cfg.CryptoKey == "" {
		cfg.LoadCryptoKey(true)
	}

	if hasKeyFile || hasKeyEncryptedFile {

		var file string
		if hasKeyEncryptedFile {
			file = immutableOptions["x-tyr-encrypted-file"].(string)
		} else {
			file = immutableOptions["file"].(string)
		}
		if len(file) > 0 && file[0:1] != "/" {
			file = cfg.DeployDir + "/" + file
		}

		bytes, err := ioutil.ReadFile(file)
		errors.Check(err)
		if hasKeyEncryptedFile {
			decrypted, err := crypto.Decrypt(cfg.CryptoKey, string(bytes))
			errors.Check(err)
			bytes = []byte(decrypted)
		}

		immutableOptions["external"] = true
		if hasKeyFile {
			delete(immutableOptions, "file")
		}

		hash := docker.HashImmutableBytes(bytes)

		immutableFullName := immutableName + "_" + hash

		var immutablesList []string
		switch immutableType {
		case docker.ImmutableConfig:
			immutablesList = docker.Configs(config.PrefixNS("", cfg.ConfigNamespace))
		case docker.ImmutableSecret:
			immutablesList = docker.Secrets(config.PrefixNS("", cfg.SecretNamespace))
		}
		if !tools.SliceContainsString(immutablesList, immutableFullName) {

			apiSubcommand := getImmutableApiSubcommand(immutableType)
			logImmutableTypeKey := getImmutableTypeKey(immutableType)

			var labelsArgs []string
			labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".auto=true"}...)
			labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".file=true"}...)

			commandArgs := []string{apiSubcommand, "create"}
			commandArgs = append(commandArgs, labelsArgs...)
			commandArgs = append(commandArgs, immutableFullName)
			commandArgs = append(commandArgs, "-")
			command := exec.Command("docker", commandArgs...)

			logStreamer, logStreamerClose := tools.LogStreamerFields(logrus.Fields{
				logImmutableTypeKey: immutableName,
			})
			defer logStreamerClose()
			command.Stdout = logStreamer
			command.Stderr = logStreamer

			command.Stdin = strings.NewReader(string(bytes))

			err := command.Run()
			errors.Check(err)
		}
	}

	immutableNameEnvKey := tools.KeyEnv(immutableName)
	immutableNameEnvKey = strings.Join([]string{immutablePrefix, "NAME", immutableNameEnvKey}, "_")
	immutableNameEnvKey = cl.PrefixEnv(immutableNameEnvKey)
	immutableNameEnvKeyPrev := immutableNameEnvKey + "_PREV"

	switch immutableNameType {
	case ImmutableNameLast:
		immutableOptions["name"] = "${" + immutableNameEnvKey + "}"
		if _, hasKey := envMap[immutableNameEnvKey]; !hasKey {
			envMap[immutableNameEnvKey] = docker.FindImmutableLatestCreated(immutableType, immutableName)
		}
	case ImmutableNamePrev:
		immutableOptions["name"] = "${" + immutableNameEnvKeyPrev + "}"
		if _, hasKey := envMap[immutableNameEnvKeyPrev]; !hasKey {
			envMap[immutableNameEnvKeyPrev] = docker.FindImmutablePreviousCreated(immutableType, immutableName)
			if envMap[immutableNameEnvKeyPrev] == "" {
				envMap[immutableNameEnvKeyPrev] = docker.FindImmutableLatestCreated(immutableType, immutableName)
			}
		}
	}

	immutableNamePrev := immutableName + "_prev"
	if _, hasKeyPrev := composeSection[immutableNamePrev]; !hasKeyPrev && hasKeyXTyrRandom && xTyrRandom.(bool) {
		immutableOptionsPrev := make(map[string]interface{})
		immutableOptionsPrev["external"] = true
		immutableOptionsPrev["name"] = "${" + immutableNameEnvKeyPrev + "}"
		immutableOptionsPrev["x-tyr-random"] = true
		if _, hasKey := envMap[immutableNameEnvKeyPrev]; !hasKey {
			envMap[immutableNameEnvKeyPrev] = docker.FindImmutablePreviousCreated(immutableType, immutableName)
			if envMap[immutableNameEnvKeyPrev] == "" {
				envMap[immutableNameEnvKeyPrev] = docker.FindImmutableLatestCreated(immutableType, immutableName)
			}
		}
		composeSection[immutableNamePrev] = immutableOptionsPrev
	}

	return immutableOptions
}

func getImmutableApiSubcommand(immutableType docker.ImmutableType) string {
	switch immutableType {
	case docker.ImmutableConfig:
		return "config"
	case docker.ImmutableSecret:
		return "secret"
	}
	return ""
}

func getImmutableTypeKey(immutableType docker.ImmutableType) string {
	switch immutableType {
	case docker.ImmutableConfig:
		return "config"
	case docker.ImmutableSecret:
		return "secret"
	}
	return ""
}

func ensureImmutableRandomExists(app App, immutableType docker.ImmutableType, immutableName string, immutableOptions map[string]interface{}) {
	apiSubcommand := getImmutableApiSubcommand(immutableType)
	logImmutableTypeKey := getImmutableTypeKey(immutableType)
	out, err := exec.Command("docker", apiSubcommand, "ls", "-q", "--format", "{{.Name}}", "--filter", `name=`+immutableName).Output()
	errors.Check(err)
	immutableFullNames := tools.ByteLinesToSlice(out)
	for _, immutableFullName := range immutableFullNames {
		if !docker.ImmutableIsHashedName(immutableFullName) || immutableFullName[0:len(immutableFullName)-docker.HashLenght-1] != immutableName {
			continue
		}
		return
	}

	randomSizeI, hasKeyRandomSize := immutableOptions["x-tyr-random-size"]
	randomPrefixI, hasKeyRandomPrefix := immutableOptions["x-tyr-random-prefix"]
	randomRegexI, hasKeyRandomRegex := immutableOptions["x-tyr-random-regex"]
	randomCharsI, hasKeyRandomChars := immutableOptions["x-tyr-random-chars"]
	randomLockI, hasKeyRandomLock := immutableOptions["x-tyr-random-lock"]

	var randomSize int
	if hasKeyRandomSize {
		randomSize = int(randomSizeI.(float64))
	} else {
		randomSize = config.FlagRandomSizeDefault
	}

	var randomLock bool
	if hasKeyRandomLock {
		randomLock = randomLockI.(bool)
	} else {
		randomLock = false
	}
	var randomLockStr string
	if randomLock {
		randomLockStr = "true"
	} else {
		randomLockStr = "false"
	}

	var randomPrefix string
	if hasKeyRandomPrefix {
		randomPrefix = randomPrefixI.(string)
	}

	var randomRegex string
	if hasKeyRandomRegex {
		randomRegex = randomRegexI.(string)
	}

	var randomChars string
	if hasKeyRandomChars {
		randomChars = randomCharsI.(string)
	}

	randomValue, err := crypto.Random(crypto.RandomConfig{
		Regex:  randomRegex,
		Chars:  randomChars,
		Size:   randomSize,
		Prefix: randomPrefix,
	})
	errors.Check(err)

	hash := docker.HashImmutableBytes([]byte(randomValue))
	immutableFullName := immutableName + "_" + hash

	if randomLock {
		cfg := app.GetConfig()
		dir := cfg.DeployLocksDir + "/secrets/" + immutableName
		os.MkdirAll(dir, os.ModePerm)
		filepath := dir + "/" + immutableFullName + ".lock"
		_, err := os.Stat(filepath)
		if os.IsNotExist(err) {
			file, err := os.Create(filepath)
			errors.Check(err)
			defer file.Close()
			logrus.Debugf("random secret rotation lock created %v", filepath)
		}
	}

	cfg := app.GetConfig()
	var labelsArgs []string
	labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".auto=true"}...)
	labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random=true"}...)
	labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.size=" + strconv.Itoa(randomSize)}...)
	labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.prefix=" + randomPrefix}...)
	labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.regex=" + randomRegex}...)
	labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.chars=" + randomChars}...)
	labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.lock=" + randomLockStr}...)

	commandArgs := []string{apiSubcommand, "create"}
	commandArgs = append(commandArgs, labelsArgs...)
	commandArgs = append(commandArgs, immutableFullName)
	commandArgs = append(commandArgs, "-")
	command := exec.Command("docker", commandArgs...)
	logStreamer, logStreamerClose := tools.LogStreamerFields(logrus.Fields{
		logImmutableTypeKey: immutableName,
	})
	defer logStreamerClose()
	command.Stdout = logStreamer
	command.Stderr = logStreamer

	command.Stdin = strings.NewReader(randomValue)

	err = command.Run()
	errors.Check(err)
}

func injectLabelUpdateImage(app App, composeFile *compose.ComposeFile, envMap map[string]string) {
	labelKey := "hash.image"
	for serviceName, service := range composeFile.Services {
		var image string
		image = service["image"].(string)
		var err error
		image, err = goenv.Expand(image, envMap)
		errors.Check(err)
		var hash string
		hash, _ = docker.ImageHash(image)
		labelEnvKey := DeployLabelEnvKey(app, labelKey+"_"+serviceName)
		injectLabelEnv(app, labelEnvKey, service, envMap, labelKey, hash)
	}
}
func injectLabelUpdateStack(app App, composeFile *compose.ComposeFile, envMap map[string]string) {
	var hash string
	var err error
	h := sha1.New()
	var bytes []byte
	bytes, err = json.Marshal(composeFile)
	errors.Check(err)
	bytes, err = yaml.JSONToYAML(bytes)
	errors.Check(err)
	h.Write(bytes)
	var str string
	str, err = goenv.Marshal(envMap)
	errors.Check(err)
	h.Write([]byte(str))
	hash = hex.EncodeToString(h.Sum(nil))
	labelKey := "hash.stack"
	labelEnvKey := DeployLabelEnvKey(app, labelKey)
	for _, service := range composeFile.Services {
		injectLabelEnv(app, labelEnvKey, service, envMap, labelKey, hash)
	}
}

func injectLabelSecrets(app App, composeFile *compose.ComposeFile, envMap map[string]string) {
	for _, service := range composeFile.Services {
		if service["secrets"] == nil {
			continue
		}
		secrets := service["secrets"].([]interface{})
		for _, secret := range secrets {
			switch secret.(type) {
			case map[string]interface{}:
				secretDef := secret.(map[string]interface{})
				secretName := secretDef["source"].(string)
				if usageI, hasKey := secretDef["x-tyr-secret-usage"]; hasKey {
					usage := usageI.(string)
					injectLabel(app, service, "secrets."+secretName+".usage", usage)
				}
			}
		}
	}
}

func injectLabelEnv(app App, labelEnvKey string, service map[string]interface{}, envMap map[string]string, key string, value string) {
	envMap[labelEnvKey] = value
	injectLabel(app, service, key, "${"+labelEnvKey+"}")
}
func injectLabel(app App, service map[string]interface{}, key string, value string) {
	cfg := app.GetConfig()
	if _, hasKey := service["labels"]; !hasKey {
		service["labels"] = make(map[string]interface{})
	}
	labels := service["labels"].(map[string]interface{})
	labels[cfg.LabelsNamespace+"."+key] = value
}

func DeployLabelEnvKey(app App, key string) string {
	cl := app.GetConfigLoader()
	cfg := app.GetConfig()
	return cl.PrefixEnv(tools.KeyEnv("DEPLOY_LABEL_" + cfg.LabelsNamespace + "_" + key))
}
