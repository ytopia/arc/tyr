package bundle

import (
	"os"
	"sync"

	cmap "github.com/orcaman/concurrent-map"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
)

func Stacks(app App, stackNames []string, force bool) cmap.ConcurrentMap {
	bundleStacks := cmap.New()
	shared := Shared(app)
	wg := &sync.WaitGroup{}
	cfg := app.GetConfig()
	os.MkdirAll(cfg.DeployBundlesDir, os.ModePerm)
	for _, stack := range stackNames {
		wg.Add(1)
		go func(stack string) {
			defer wg.Done()
			var composeFile *compose.ComposeFile
			var err error
			composeFile, err = Stack(app, stack, shared, force)
			errors.Check(err)
			bundleStacks.Set(stack, composeFile)
		}(stack)
	}
	wg.Wait()
	return bundleStacks
}
