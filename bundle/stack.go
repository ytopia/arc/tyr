package bundle

import (
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/environ"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func Stack(app App, stack string, shared *compose.ComposeFile, force bool) (*compose.ComposeFile, error) {
	var envMap = EnvInit(app, stack)

	var err error
	var composeFile *compose.ComposeFile

	cfg := app.GetConfig()
	stackFileTarget := cfg.DeployBundlesDir + "/" + stack + ".yml"
	if !force {
		if exists, _ := tools.FileExists(stackFileTarget); exists {
			return composeFile, err
		}
	}

	composeFile, err = Compose(app, stack, envMap, shared)
	errors.Check(err)

	err = EnvWrite(app, stack, envMap)
	errors.Check(err)

	if force && cfg.BundleValidate {
		envMap := environ.StackEnvMapFromBundle(app, stack)
		err = compose.Validate(cfg.DeployDir, stackFileTarget, envMap)
		if err != nil {
			if err := os.Remove(stackFileTarget); err == nil {
				logrus.Debugf("invalid bundle compose file %v deleted", stackFileTarget)
			}
			logrus.Fatal(err)
		}
	}

	return composeFile, nil
}
