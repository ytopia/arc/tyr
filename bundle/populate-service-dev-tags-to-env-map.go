package bundle

import "gitlab.com/youtopia.earth/ops/tyr/compose"

func populateServiceDevTagsToEnvMap(composeFile *compose.ComposeFile, envMap map[string]string, devTag string) {
	services := make([]string, len(composeFile.Services))
	i := 0
	for k := range composeFile.Services {
		services[i] = k
		i++
	}
	compose.PopulateServiceDevTagsToEnvMap(services, envMap, devTag)
}
