package bundle

import (
	"github.com/ghodss/yaml"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/xjsonnet"
)

func Shared(app App) *compose.ComposeFile {
	cfg := app.GetConfig()

	var err error

	json, err := xjsonnet.Render(cfg.SharedFile)
	errors.Check(err)

	composeFile := &compose.ComposeFile{}
	err = yaml.Unmarshal([]byte(json), composeFile)
	errors.Check(err)

	return composeFile
}
