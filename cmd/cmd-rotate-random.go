package cmd

import (
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/copier"
	cmap "github.com/orcaman/concurrent-map"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/youtopia.earth/ops/tyr/bundle"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/crypto"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
	yaml "gopkg.in/yaml.v2"
)

func CmdRotateRandom(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "rotate-random [SECRET...]",
		Short: "rotate random secrets",
		Args:  cobra.RangeArgs(0, 1),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			cfg := app.GetConfig()

			var rootNames []string
			if len(args) == 0 {
				rootNames = docker.SecretRootNames(config.PrefixNS("", cfg.SecretNamespace))
			} else {
				for _, arg := range args {
					rootNames = append(rootNames, config.PrefixNS(arg, cfg.SecretNamespace))
				}
			}

			for _, rootName := range rootNames {
				secretFullName := docker.FindImmutableLatestCreated(docker.ImmutableSecret, rootName)

				if docker.SecretLabel(secretFullName, cfg.LabelsNamespace+".random") != "true" {
					continue
				}

				createdDate := docker.SecretCreatedTime(secretFullName)
				expireTime := createdDate.Add(cfg.RotateRandomInterval)
				expired := time.Now().After(expireTime)

				if !(expired || cfg.RotateRandomForce) {
					continue
				}

				randomRegex := docker.SecretLabel(secretFullName, cfg.LabelsNamespace+".random.regex")
				randomChars := docker.SecretLabel(secretFullName, cfg.LabelsNamespace+".random.chars")
				randomPrefix := docker.SecretLabel(secretFullName, cfg.LabelsNamespace+".random.prefix")

				var randomLock bool
				randomLockStr := docker.SecretLabel(secretFullName, cfg.LabelsNamespace+".random.lock")
				if randomLockStr == "true" {
					randomLock = true
				} else if randomLockStr == "false" {
					randomLock = true
				} else {
					randomLock = cfg.RandomLock
					if randomLock {
						randomLockStr = "true"
					} else {
						randomLockStr = "false"
					}
				}

				if randomLock {
					filepath := cfg.DeployLocksDir + "/secrets/" + rootName + "/" + secretFullName + ".lock"
					_, err := os.Stat(filepath)
					if !os.IsNotExist(err) && !cfg.RotateRandomForceLock {
						logrus.Debugf(`secret rotation of "%v" is locked, skipping`, rootName)
						continue
					}
				}

				var randomSize int
				randomSizeStr := docker.SecretLabel(secretFullName, cfg.LabelsNamespace+".random.size")
				if randomSizeStr == "" {
					randomSize = cfg.RandomSize
					randomRegex = cfg.RandomRegex
					randomChars = cfg.RandomChars
					randomPrefix = cfg.RandomPrefix
				} else {
					var err error
					randomSize, err = strconv.Atoi(randomSizeStr)
					errors.Check(err)
				}

				logrus.Infof(`generating new random value for secret "%v"`, rootName)

				content, err := crypto.Random(crypto.RandomConfig{
					Regex:  randomRegex,
					Chars:  randomChars,
					Size:   randomSize,
					Prefix: randomPrefix,
				})
				errors.Check(err)

				newSecretFullName := rootName + "_" + docker.HashImmutableBytes([]byte(content))

				if randomLock {
					dir := cfg.DeployLocksDir + "/secrets/" + rootName
					os.MkdirAll(dir, os.ModePerm)
					filepath := dir + "/" + newSecretFullName + ".lock"
					_, err := os.Stat(filepath)
					if os.IsNotExist(err) {
						file, err := os.Create(filepath)
						errors.Check(err)
						defer file.Close()
						logrus.Debugf("random secret rotation lock created %v", filepath)
					}
				}

				var labelsArgs []string
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".auto=true"}...)
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random=true"}...)
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.size=" + strconv.Itoa(randomSize)}...)
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.regex=" + randomRegex}...)
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.chars=" + randomChars}...)
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.prefix=" + randomPrefix}...)
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.lock=" + randomLockStr}...)

				commandArgs := []string{"secret", "create"}
				commandArgs = append(commandArgs, labelsArgs...)
				commandArgs = append(commandArgs, newSecretFullName)
				commandArgs = append(commandArgs, "-")
				command := exec.Command("docker", commandArgs...)

				command.Stdin = strings.NewReader(content)
				err = command.Run()
				errors.Check(err)

			}

			if !cfg.RotateRandomUpdateServices {
				return
			}

			allServices, err := docker.ServicesPrefixed(cfg.StackNamespace)
			errors.Check(err)

			var upServices []string
			serverSecretServices := make(map[string][]string)
			clientSecretServices := make(map[string][]string)

			for _, service := range allServices {
				out, err := exec.Command("docker", "service", "inspect", "--format", "{{json .Spec.TaskTemplate.ContainerSpec.Secrets}}", service).Output()
				errors.Check(err)
				raw := strings.TrimRight(string(out), "\n")
				if raw == "" || raw == "null" {
					continue
				}
				var secrets []map[string]interface{}
				err = yaml.Unmarshal([]byte(raw), &secrets)
				errors.Check(err)

				var secretRootNames []string
				for _, secretInfos := range secrets {
					secretName := secretInfos["SecretName"].(string)
					secretRootName := docker.ImmutableGetRootName(secretName)
					secretRootNames = append(secretRootNames, secretRootName)
				}

				for _, secretRootName := range secretRootNames {

					out, err := exec.Command("docker", "service", "inspect", "--format", `{{index .Spec.TaskTemplate.ContainerSpec.Labels "`+cfg.LabelsNamespace+`.secrets.`+secretRootName+`.usage"}}`, service).Output()
					errors.Check(err)
					secretUsage := strings.TrimRight(string(out), "\n")

					if !tools.SliceContainsString(upServices, service) {
						upServices = append(upServices, service)
					}
					switch secretUsage {
					case "":
					case "server":
						if !tools.SliceContainsString(serverSecretServices[secretRootName], service) {
							serverSecretServices[secretRootName] = append(serverSecretServices[secretRootName], service)
						}
					case "client":
						if !tools.SliceContainsString(clientSecretServices[secretRootName], service) {
							clientSecretServices[secretRootName] = append(clientSecretServices[secretRootName], service)
						}
					default:
						logrus.Errorf("Found unkown secret usage: %v for secret %v used by service %v", secretUsage, secretRootName, service)
					}
				}

			}

			var stackNames []string
			stackNameByService := make(map[string]string)
			for _, service := range upServices {
				out, err := exec.Command("docker", "service", "inspect", "--format", `{{index .Spec.TaskTemplate.ContainerSpec.Labels "com.docker.stack.namespace"}}`, service).Output()
				errors.Check(err)
				stackName := strings.TrimRight(string(out), "\n")
				if cfg.StackNamespace != "" {
					stackName = stackName[len(cfg.StackNamespace)+1:]
				}
				if !tools.SliceContainsString(stackNames, stackName) {
					stackNames = append(stackNames, stackName)
				}
				stackNameByService[service] = stackName
			}

			var bundleStacks cmap.ConcurrentMap
			if cfg.Bundle {
				bundleStacks = bundle.Stacks(app, stackNames, cfg.ForceBundle)
			}

			restartService := func(service string, serviceType string) {
				stackName := stackNameByService[service]
				serviceShortname := service
				if cfg.StackNamespace != "" {
					serviceShortname = serviceShortname[len(cfg.StackNamespace)+1:]
				}
				serviceShortname = serviceShortname[len(stackName)+1:]

				logrus.Infof("restarting %v service %v of stack %v", serviceType, serviceShortname, stackName)

				composeFileOriginI, _ := bundleStacks.Get(stackName)
				composeFileOrigin := composeFileOriginI.(*compose.ComposeFile)
				composeFile := &compose.ComposeFile{}
				copier.Copy(composeFile, &composeFileOrigin)
				services := make(map[string]map[string]interface{})
				for k := range composeFile.Services {
					if k == serviceShortname {
						services[k] = composeFile.Services[k]
					}
				}
				composeFile.Services = services

				composeFileBytes, err := bundle.ComposeFileToYAML(composeFile)
				if err != nil {
					logrus.Fatal(err)
				}

				compose.DeployStackContent(app, stackName, composeFileBytes)
				// logrus.Infof("%v %v", stackName, composeFile.Services)

			}

			for _, secretRootName := range rootNames {
				serverServices := serverSecretServices[secretRootName]
				for _, service := range serverServices {
					restartService(service, "server")
				}
				CommonWaitServices(app, serverServices)

				for _, service := range clientSecretServices[secretRootName] {
					restartService(service, "client")
				}
			}

		},
	}
	flags := cmd.Flags()
	flags.BoolP("force", "f", config.FlagRotateRandomForceDefault, config.FlagRotateRandomForceDesc)
	flags.Bool("force-lock", config.FlagRotateRandomForceLockDefault, config.FlagRotateRandomForceLockDesc)
	flags.StringP("interval", "i", config.FlagRotateRandomIntervalDefault, config.FlagRotateRandomIntervalDesc)
	flags.StringP("labels-namespace", "", config.FlagLabelsNamespaceDefault, config.FlagLabelsNamespaceDesc)
	flags.Int("random-size", config.FlagRandomSizeDefault, config.FlagRandomSizeDesc)
	flags.String("random-prefix", config.FlagRandomPrefixDefault, config.FlagRandomPrefixDesc)
	flags.String("random-chars", config.FlagRandomCharsDefault, config.FlagRandomCharsDesc)
	flags.Bool("random-lock", config.FlagRandomLockDefault, config.FlagRandomLockDesc)
	flags.BoolP("update-services", "", config.FlagRotateRandomUpdateServicesDefault, config.FlagRotateRandomUpdateServicesDesc)

	// bundle
	flags.BoolP("bundle", "", config.FlagBundleDefault, config.FlagBundleDesc)
	flags.BoolP("bundle-validate", "", config.FlagBundleValidateDefault, config.FlagBundleValidateDesc)
	flags.BoolP("bundle-update-image", "", config.FlagBundleUpdateImageDefault, config.FlagBundleUpdateImageDesc)
	flags.BoolP("bundle-update-stack", "", config.FlagBundleUpdateStackDefault, config.FlagBundleUpdateStackDesc)
	flags.BoolP("force-bundle", "", true, config.FlagForceBundleDesc)
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.VarP(&FlagValueBoolPlusAuto{Value: config.FlagServiceDevTagDefault}, "service-dev-tag", "", config.FlagServiceDevTagDesc)
	flags.StringP("shared-file", "", config.FlagSharedFileDefault, config.FlagSharedFileDesc)
	flags.StringP("shared-env-dir", "", config.FlagSharedEnvDirDefault, config.FlagSharedEnvDirDesc)
	flags.StringP("compose-version", "", config.FlagComposeVersionDefault, config.FlagComposeVersionDesc)
	flags.StringP("deploy-bundles-dir", "", config.FlagDeployBundlesDirDefault, config.FlagDeployBundlesDirDesc)
	flags.StringP("deploy-locks-dir", "", config.FlagDeployLocksDirDefault, config.FlagDeployLocksDirDesc)
	flags.StringP("crypto-key", "", config.FlagCryptoKeyDefault, config.FlagCryptoKeyDesc)
	flags.StringP("crypto-key-file", "", config.FlagCryptoKeyFileDefault, config.FlagCryptoKeyFileDesc)
	flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)

	return cmd
}
