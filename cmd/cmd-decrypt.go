package cmd

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/crypto"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
)

func CmdDecrypt(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "decrypt [file|-] [DECRYPTED_FILE]",
		Short: "decrypt file",
		Args:  cobra.RangeArgs(0, 2),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()
			cfg.LoadCryptoKey(true)

			var err error
			var content string
			if len(args) == 0 {
				if cfg.DecryptData == "" {
					logrus.Fatal("missing decrypt data, --data flag or -|file argument")
				}
				content = cfg.DecryptData
			} else if args[0] == "-" {
				var contentB []byte
				contentB, err = ioutil.ReadAll(os.Stdin)
				errors.Check(err)
				content = string(contentB)
			} else {
				var contentB []byte
				contentB, err = ioutil.ReadFile(args[0])
				errors.Check(err)
				content = string(contentB)
			}

			decrypted, err := crypto.Decrypt(cfg.CryptoKey, content)
			errors.Check(err)

			if len(args) < 2 {
				fmt.Println(decrypted)
				return
			}

			decryptedFile := args[1]
			err = ioutil.WriteFile(decryptedFile, []byte(decrypted), 0644)
			errors.Check(err)

		},
	}
	flags := cmd.Flags()
	flags.StringP("crypto-key", "", config.FlagCryptoKeyDefault, config.FlagCryptoKeyDesc)
	flags.StringP("crypto-key-file", "", config.FlagCryptoKeyFileDefault, config.FlagCryptoKeyFileDesc)
	flags.StringP("data", "d", "", config.FlagDecryptDataDesc)
	return cmd
}
