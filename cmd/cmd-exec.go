package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func CmdExec(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "exec [command-options] service [container] [--] [docker-exec-options] command",
		Short: "docker exec user friendly",
		Args:  cobra.MinimumNArgs(2),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			argsP := &args
			containerFullName := CommonExecContainerName(app, cmd, argsP)
			command := CommonExecCommand(app, cmd, argsP, containerFullName)
			if command == nil {
				return
			}
			command.Stdout = os.Stdout
			command.Stderr = os.Stderr
			command.Stdin = os.Stdin
			if err := command.Run(); err != nil {
				logrus.Fatal(err)
			}
		},
	}
	flags := cmd.Flags()
	flags.SetInterspersed(false)
	return cmd
}
