package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/crypto"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
)

func CmdRandom(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "random",
		Short: "generate random string",
		Args:  cobra.ExactArgs(0),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()

			val, err := crypto.Random(crypto.RandomConfig{
				Regex:  cfg.RandomRegex,
				Chars:  cfg.RandomChars,
				Size:   cfg.RandomSize,
				Prefix: cfg.RandomPrefix,
			})
			errors.Check(err)

			fmt.Println(val)

		},
	}

	flags := cmd.Flags()
	flags.Int("size", config.FlagRandomSizeDefault, config.FlagRandomSizeDesc)
	flags.String("prefix", config.FlagRandomPrefixDefault, config.FlagRandomPrefixDesc)
	flags.String("regex", config.FlagRandomRegexDefault, config.FlagRandomRegexDesc)
	flags.String("chars", config.FlagRandomCharsDefault, config.FlagRandomCharsDesc)

	return cmd
}
