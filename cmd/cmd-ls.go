package cmd

import (
	"github.com/spf13/cobra"
)

func CmdLs(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "ls",
		Short: "display lists from config or ressources",
	}
	cmd.AddCommand(CmdLsBuilds(app))
	cmd.AddCommand(CmdLsConfigs(app))
	cmd.AddCommand(CmdLsSecrets(app))
	cmd.AddCommand(CmdLsServices(app))
	cmd.AddCommand(CmdLsStacks(app))
	cmd.AddCommand(CmdLsConfigKeys(app))
	return cmd
}
