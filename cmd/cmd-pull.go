package cmd

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/bundle"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/prompts"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdPull(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "pull [stacks]",
		Short: "pull images required by stacks's services",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()

			var stackNames []string
			if len(args) > 0 {
				stackNames = args
			} else {
				stackNames = prompts.StacksMultiSelectFull(app, cmd, `pull images for all stacks %v ?`, "yes")
			}
			if len(stackNames) == 0 {
				return
			}

			if cfg.Bundle {
				bundle.Stacks(app, stackNames, cfg.ForceBundle)
			}

			ignoreTags := cfg.PullIgnoreTags

			if cfg.PullIgnoreDevTag {
				cfg := app.GetConfig()
				ignoreTags = append(ignoreTags, cfg.DevTag)
			}

			var pullImages []string
			for _, stack := range stackNames {
				images := compose.StackRequiredImages(app, stack)
				for _, image := range images {
					parts := strings.Split(image, ":")
					if len(parts) > 1 {
						imageTag := parts[len(parts)-1]
						if !tools.SliceContainsString(ignoreTags, imageTag) {
							pullImages = append(pullImages, image)
						}
					} else {
						pullImages = append(pullImages, image)
					}
				}
			}

			for _, pullImage := range pullImages {
				logrus.Infof("%v", pullImage)
			}

			docker.PullImages(pullImages, cfg.PullForce)

		},
	}

	flags := cmd.Flags()
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.BoolP("force", "f", config.FlagForceDefault, config.FlagForcePullDesc)
	flags.StringSlice("ignore-tags", config.FlagIgnoreTagsDefault, config.FlagIgnoreTagsDesc)
	flags.BoolP("ignore-dev-tag", "", config.FlagIgnoreDevTagDefault, config.FlagIgnoreDevTagDesc)
	flags.StringP("deploy-bundles-dir", "", config.FlagDeployBundlesDirDefault, config.FlagDeployBundlesDirDesc)
	flags.StringP("deploy-locks-dir", "", config.FlagDeployLocksDirDefault, config.FlagDeployLocksDirDesc)
	flags.StringP("shared-file", "", config.FlagSharedFileDefault, config.FlagSharedFileDesc)
	flags.StringP("shared-env-dir", "", config.FlagSharedEnvDirDefault, config.FlagSharedEnvDirDesc)
	flags.StringP("compose-version", "", config.FlagComposeVersionDefault, config.FlagComposeVersionDesc)
	flags.VarP(&FlagValueBoolPlusAuto{Value: config.FlagServiceDevTagDefault}, "service-dev-tag", "", config.FlagServiceDevTagDesc)
	flags.BoolP("bundle", "", config.FlagBundleDefault, config.FlagBundleDesc)
	flags.BoolP("bundle-validate", "", config.FlagBundleValidateDefault, config.FlagBundleValidateDesc)
	flags.BoolP("bundle-update-image", "", config.FlagBundleUpdateImageDefault, config.FlagBundleUpdateImageDesc)
	flags.BoolP("bundle-update-stack", "", config.FlagBundleUpdateStackDefault, config.FlagBundleUpdateStackDesc)
	flags.StringP("labels-namespace", "", config.FlagLabelsNamespaceDefault, config.FlagLabelsNamespaceDesc)
	flags.BoolP("force-bundle", "", true, config.FlagForceBundleDesc)
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "pull all"))

	flags.String("network-namespace", "", config.FlagNetworkNamespaceDesc)
	flags.String("volume-namespace", "", config.FlagVolumeNamespaceDesc)
	flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)
	flags.String("config-namespace", "", config.FlagConfigNamespaceDesc)

	return cmd
}
