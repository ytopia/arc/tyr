package cmd

import (
	"os/exec"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdPs(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "ps",
		Short: "docker stack ps",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cobraCmd *cobra.Command, args []string) {
			cfg := app.GetConfig()

			stack := cfg.Stack

			var stacks []string
			var opts []string
			for _, arg := range args {
				if arg[0:1] == "-" {
					opts = append(opts, arg)
				} else {
					stacks = append(stacks, arg)
				}
			}
			if len(stacks) == 0 && stack != "" {
				stacks = append(stacks, stack)
			}
			if len(stacks) == 0 {
				stacks = compose.GetStackNames(app)
			}

			if noTrunc, _ := cobraCmd.Flags().GetBool("no-trunc"); noTrunc {
				opts = append(opts, "--no-trunc")
			}

			for _, stack := range stacks {
				commandArgs := []string{"stack", "ps", config.PrefixNS(stack, cfg.StackNamespace)}
				commandArgs = append(commandArgs, opts...)
				command := exec.Command("docker", commandArgs...)

				logStreamer, logStreamerClose := tools.LogStreamer()
				defer logStreamerClose()
				command.Stdout = logStreamer
				command.Stderr = logStreamer

				command.Run()
			}
		},
	}

	flags := cmd.Flags()
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.BoolP("no-trunc", "", config.FlagNoTruncDefault, config.FlagNoTruncDesc)
	flags.SetInterspersed(false)

	return cmd
}
