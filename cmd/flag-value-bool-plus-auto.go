package cmd

import (
	"fmt"

	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

type FlagValueBoolPlusAuto struct {
	Value string
}

func (f *FlagValueBoolPlusAuto) String() string {
	return f.Value
}

var ServiceDevTagValues = []string{"auto", "true", "false"}

func (f *FlagValueBoolPlusAuto) Set(val string) error {
	f.Value = val
	if !tools.SliceContainsString(ServiceDevTagValues, val) {
		return fmt.Errorf("Unexpected value %v for service-dev-tag, possible values: %v", val, ServiceDevTagValues)
	}
	return nil
}
func (f *FlagValueBoolPlusAuto) Type() string {
	return "bool-plus-auto"
}
