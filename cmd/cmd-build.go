package cmd

import (
	"fmt"
	"sync"

	survey "github.com/AlecAivazis/survey/v2"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/prompts"
	"gitlab.com/youtopia.earth/ops/tyr/tools"

	cmap "github.com/orcaman/concurrent-map"
	"github.com/spf13/cobra"
)

func CmdBuild(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "build [tyr options] [SERVICE...] [--] [docker-compose build options]",
		Short: "docker-compose build",
		Long:  `wrapper for docker-compose build with dependencies resolution and environment variables injection`,
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			cfg := app.GetConfig()

			// select stacks
			var stackNames []string
			if len(args) > 0 {
				stackNames = args
			} else {
				stackNames = prompts.StacksMultiSelectFull(app, cmd, `build all stacks %v ?`, "yes")
			}
			if len(stackNames) == 0 {
				return
			}

			var buildNames []string
			var buildOpts []string

			for _, arg := range args {
				if arg[0:1] == "-" {
					if arg != "--" {
						buildOpts = append(buildOpts, arg)
					}
				} else {
					buildNames = append(buildNames, arg)
				}
			}

			for _, arg := range cfg.BuildParameters {
				if arg != "" {
					buildOpts = append(buildOpts, arg)
				}
			}

			if len(buildNames) == 0 {
				for _, stack := range stackNames {
					for _, buildName := range compose.StackGetBuildNames(app, stack) {
						if !tools.SliceContainsString(buildNames, buildName) {
							buildNames = append(buildNames, buildName)
						}
					}
				}

				if !cfg.Yes {

					var buildAll string
					prompt := &survey.Select{
						Message: fmt.Sprintf(`build all services %v ?`, buildNames),
						Options: []string{"yes", "no", "cancel"},
						Default: "no",
					}
					survey.AskOne(prompt, &buildAll)

					if buildAll == "cancel" || buildAll == "" {
						return
					}
					if buildAll == "no" {
						selectedBuilds := []string{}
						prompt := &survey.MultiSelect{
							Message: "select builds:",
							Options: buildNames,
						}
						survey.AskOne(prompt, &selectedBuilds)

						if len(selectedBuilds) == 0 {
							return
						}

						buildNames = selectedBuilds
					}
				}
			}

			buildTasks := cmap.New()
			wg := &sync.WaitGroup{}

			for _, buildName := range buildNames {
				wg.Add(1)
				buildOptions := &compose.BuildOptions{
					App:              app,
					Name:             buildName,
					CleanEnv:         cfg.BuildCleanEnv,
					BuildMissingDeps: cfg.BuildMissingDeps,
					BuildDeps:        cfg.BuildDeps,
					CommandArgs:      buildOpts,
					Tasks:            buildTasks,
				}
				go func(buildOptions *compose.BuildOptions) {
					defer wg.Done()
					compose.Build(buildOptions)
				}(buildOptions)
			}
			wg.Wait()

		},
	}

	flags := cmd.Flags()
	flags.BoolP("clean-env", "", config.FlagBuildCleanEnvDefault, config.FlagBuildCleanEnvDesc)
	flags.StringP("compose-file", "c", config.FlagBuildComposeFileDefault, config.FlagBuildCleanEnvDesc)
	flags.StringP("builds-dir", "", config.FlagBuildsDirDefault, config.FlagBuildsDirDesc)
	flags.BoolP("deps", "", config.FlagBuildDepsDefault, config.FlagBuildDepsDesc)
	flags.BoolP("missing-deps", "", config.FlagBuildMissingDepsDefault, config.FlagBuildMissingDepsDesc)
	flags.StringSlice("parameters", config.FlagBuildParametersDefault, config.FlagBuildParametersDesc)
	flags.StringP("deploy-bundles-dir", "", config.FlagDeployBundlesDirDefault, config.FlagDeployBundlesDirDesc)
	flags.StringP("deploy-locks-dir", "", config.FlagDeployLocksDirDefault, config.FlagDeployLocksDirDesc)
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "build all"))
	flags.VarP(&FlagValueBoolPlusAuto{Value: config.FlagServiceDevTagDefault}, "service-dev-tag", "", config.FlagServiceDevTagDesc)

	return cmd
}
