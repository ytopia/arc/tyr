package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab.com/youtopia.earth/ops/tyr/bundle"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/prompts"
)

func CmdBundle(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "bundle",
		Short: "build bundle docker stack compose files and env files",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			var stackNames []string
			if len(args) > 0 {
				stackNames = args
			} else {
				stackNames = prompts.StacksMultiSelectFull(app, cmd, `bundle all stacks %v ?`, "yes")
			}
			if len(stackNames) == 0 {
				return
			}

			bundle.Stacks(app, stackNames, true)

		},
	}

	flags := cmd.Flags()
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.VarP(&FlagValueBoolPlusAuto{Value: config.FlagServiceDevTagDefault}, "service-dev-tag", "", config.FlagServiceDevTagDesc)
	flags.StringP("shared-file", "", config.FlagSharedFileDefault, config.FlagSharedFileDesc)
	flags.StringP("shared-env-dir", "", config.FlagSharedEnvDirDefault, config.FlagSharedEnvDirDesc)
	flags.StringP("compose-version", "", config.FlagComposeVersionDefault, config.FlagComposeVersionDesc)
	flags.StringP("deploy-bundles-dir", "", config.FlagDeployBundlesDirDefault, config.FlagDeployBundlesDirDesc)
	flags.StringP("deploy-locks-dir", "", config.FlagDeployLocksDirDefault, config.FlagDeployLocksDirDesc)
	flags.BoolP("update-image", "", config.FlagBundleUpdateImageDefault, config.FlagBundleUpdateImageDesc)
	flags.BoolP("update-stack", "", config.FlagBundleUpdateStackDefault, config.FlagBundleUpdateStackDesc)
	flags.StringP("labels-namespace", "", config.FlagLabelsNamespaceDefault, config.FlagLabelsNamespaceDesc)
	flags.BoolP("validate", "", config.FlagBundleValidateDefault, config.FlagBundleValidateDesc)
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "bundle all"))
	flags.StringP("crypto-key", "", config.FlagCryptoKeyDefault, config.FlagCryptoKeyDesc)
	flags.StringP("crypto-key-file", "", config.FlagCryptoKeyFileDefault, config.FlagCryptoKeyFileDesc)

	flags.String("network-namespace", "", config.FlagNetworkNamespaceDesc)
	flags.String("volume-namespace", "", config.FlagVolumeNamespaceDesc)
	flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)
	flags.String("config-namespace", "", config.FlagConfigNamespaceDesc)

	return cmd
}
