package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
)

func CmdReadSecret(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "read-secret [SecretNames]",
		Short: "cheating to read secret value",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			for _, secretName := range args {
				secretValue := docker.GetSecret(secretName)
				fmt.Println(secretValue)
			}

		},
	}

	// flags := cmd.Flags()
	// flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)

	return cmd
}
