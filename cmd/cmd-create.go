package cmd

import (
	"github.com/spf13/cobra"
)

func CmdCreate(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "create",
		Short: "create immutables with hash suffix (configs and secrets)",
	}
	cmd.AddCommand(CmdCreateConfig(app))
	cmd.AddCommand(CmdCreateSecret(app))
	return cmd
}
