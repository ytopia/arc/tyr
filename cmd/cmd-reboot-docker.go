package cmd

import (
	"os"
	"os/exec"
	"strings"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdRebootDocker(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "reboot-docker",
		Short: "restart docker daemon with bugfixs",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()
			sucmd := cfg.Sucmd

			runCmd := func(commandSlice ...string) error {
				command := exec.Command(commandSlice[0], commandSlice[1:]...)
				command.Stdin = os.Stdin

				logStreamer, logStreamerClose := tools.LogStreamer()
				defer logStreamerClose()
				command.Stdout = logStreamer
				command.Stderr = logStreamer

				logrus.Debugf("%v", strings.Join(commandSlice, " "))
				return command.Run()
			}

			if err := runCmd(sucmd, "/etc/init.d/docker", "stop"); err != nil {
				logrus.Fatal(err)
			}

			var netnsList []string
			if out, err := exec.Command(sucmd, "ls", "/var/run/docker/netns").Output(); err != nil {
				logrus.Fatal(err)
			} else {
				netnsList = tools.ByteLinesToSlice(out)
			}
			for _, netns := range netnsList {
				var command *exec.Cmd
				var commandBin string
				var commandArgs []string

				netnsPath := "/var/run/docker/netns/" + netns

				commandBin = sucmd
				commandArgs = []string{"umount", netnsPath}
				command = exec.Command(commandBin, commandArgs...)
				command.Stdin = os.Stdin

				logStreamer, logStreamerClose := tools.LogStreamer()
				defer logStreamerClose()
				command.Stdout = logStreamer

				logrus.Debugf("%v %v", commandBin, strings.Join(commandArgs, " "))
				if err := command.Run(); err != nil {
					if exiterr, ok := err.(*exec.ExitError); ok {
						if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
							exitCode := status.ExitStatus()
							if exitCode != 32 { // tried to unmount not mounted
								logrus.Error(err)
							}
						}
					}
				}

				commandBin = sucmd
				commandArgs = []string{"rm", netnsPath}
				command = exec.Command(commandBin, commandArgs...)
				command.Stdin = os.Stdin

				logStreamer, logStreamerClose = tools.LogStreamer()
				defer logStreamerClose()
				command.Stdout = logStreamer
				command.Stderr = logStreamer

				logrus.Debugf("%v %v", commandBin, strings.Join(commandArgs, " "))
				if err := command.Run(); err != nil {
					logrus.Fatal(err)
				}
			}

			var ifaceList []string
			if out, err := exec.Command("ip", "link", "show").Output(); err != nil {
				logrus.Fatal(err)
			} else {
				ifaceList = tools.ByteLinesToSlice(out)
			}
			for _, iface := range ifaceList {
				parts := strings.Split(iface, " ")
				if len(parts[1]) > 0 {
					ifaceName := parts[1][0 : len(parts[1])-1]
					if len(ifaceName) >= 3 && ifaceName[0:3] == "vx-" {
						runCmd(sucmd, "ip", "link", "delete", ifaceName)
					}
				}
			}

			if err := runCmd(sucmd, "/etc/init.d/docker", "start"); err != nil {
				logrus.Fatal(err)
			}
		},
	}

	flags := cmd.Flags()
	flags.StringP("sucmd", "", config.FlagSudoDefault, config.FlagSudoDesc)

	return cmd
}
