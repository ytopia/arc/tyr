package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
)

func CmdCleanSecrets(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "secrets [SecretRootNames]",
		Short: "remove swarm's unused immutables secrets",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()
			keepLatest := cfg.CleanSecretsKeepLatest

			var configSelectedNames []string
			for _, arg := range args {
				configSelectedNames = append(configSelectedNames, arg)
			}
			if len(configSelectedNames) == 0 {
				docker.CleanImmutableByPrefix(docker.ImmutableSecret, config.PrefixNS("", cfg.SecretNamespace), keepLatest)
			} else {
				for _, configSelectedName := range configSelectedNames {
					docker.CleanImmutable(docker.ImmutableSecret, config.PrefixNS(configSelectedName, cfg.SecretNamespace), keepLatest)
				}
			}

		},
	}

	flags := cmd.Flags()
	flags.BoolP("keep-latest", "", config.FlagKeepLatestDefault, config.FlagKeepLatestDesc)
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "clean all secrets"))
	flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)

	return cmd
}
