package cmd

import (
	"github.com/spf13/cobra"
	// "gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdAnd(app App, cmdRoot *cobra.Command) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "and -- COMMAND1 - COMMAND2",
		Short: "run multiple tyr commands separated by -",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cobraCmd *cobra.Command, args []string) {
			var cmdParts []string
			var cmdList [][]string
			for _, arg := range args {
				if arg == "-" {
					if len(cmdParts) > 0 {
						cmdList = append(cmdList, cmdParts)
						cmdParts = []string{}
					}
				} else {
					cmdParts = append(cmdParts, arg)
				}
			}
			cmdList = append(cmdList, cmdParts)

			for _, cmdParts := range cmdList {
				cmdRoot.SetArgs(cmdParts)
				cmdRoot.Execute()
			}
		},
	}
	flags := cmd.Flags()
	flags.SetInterspersed(false)
	return cmd
}
