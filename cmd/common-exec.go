package cmd

import (
	"os/exec"
	"strings"

	survey "github.com/AlecAivazis/survey/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CommonExecCommand(app App, cmd *cobra.Command, argsP *[]string, containerFullName string) *exec.Cmd {

	args := *argsP
	if len(args) > 0 && args[0] == "--" {
		args = args[1:]
		*argsP = args
	}

	var execOpts []string
	var execArgs []string

	var inDockerExecOpts = true
	for _, arg := range args {
		if inDockerExecOpts && arg[0:1] == "-" {
			execOpts = append(execOpts, arg)
		} else {
			inDockerExecOpts = false
			execArgs = append(execArgs, arg)
		}
	}

	commandArgs := []string{"exec"}
	commandArgs = append(commandArgs, execOpts...)
	commandArgs = append(commandArgs, containerFullName)
	commandArgs = append(commandArgs, execArgs...)
	command := exec.Command("docker", commandArgs...)

	return command
}

func CommonExecContainerName(app App, cmd *cobra.Command, argsP *[]string) string {
	cfg := app.GetConfig()
	stack := cfg.Stack

	var service string
	var containerSuffix string
	// [service] [container] [--] [docker-exec-options] [command]

	args := *argsP
	if len(args) > 0 && args[0] != "--" {
		arg := args[0]
		args = args[1:]
		if strings.Contains(arg, ".") {
			parts := strings.Split(arg, ".")
			service = parts[0]
			containerSuffix = strings.Join(parts[1:], ".")
		} else {
			service = arg
			if len(args) > 0 && args[0] != "--" {
				containerSuffix = args[0]
				args = args[1:]
			}
		}
	}
	*argsP = args

	var err error

	if stack == "" {
		var allServices []string
		if service != "" {
			allServices, err = docker.ServicesPrefixed(cfg.StackNamespace)
			errors.Check(err)
		}
		if service == "" || !tools.SliceContainsString(allServices, service) {
			var stacks []string
			stacks, err = docker.StacksPrefixed(cfg.StackNamespace)
			errors.Check(err)

			if cfg.StackNamespace != "" {
				l := len(cfg.StackNamespace) + 1
				for i, stackName := range stacks {
					stacks[i] = stackName[l:]
				}
			}

			prompt := &survey.Select{
				Message: "select stack",
				Options: stacks,
			}
			survey.AskOne(prompt, &stack)
			if stack == "" {
				return ""
			}
		}
	}

	if service == "" {

		var services []string
		services, err = docker.ServicesByStack(config.PrefixNS(stack, cfg.StackNamespace))
		errors.Check(err)

		if len(services) == 0 {
			logrus.Fatalf("services not found for stack %v", stack)
		}

		if len(services) == 1 {
			service = services[0]
			logrus.Debugf("selected service: %v", service)
		} else {
			prompt := &survey.Select{
				Message: "select service",
				Options: services,
			}
			survey.AskOne(prompt, &service)
			if service == "" {
				return ""
			}
		}

	}

	var serviceFullName string
	if stack == "" {
		serviceFullName = service
	} else {
		serviceFullName = config.PrefixNS(stack, cfg.StackNamespace) + "_" + service
	}

	if containerSuffix == "" {
		var containers []string
		containers, err = docker.ContainersByService(serviceFullName)
		errors.Check(err)

		if len(containers) == 0 {
			logrus.Fatalf("containers not found for service %v", serviceFullName)
		}
		if len(containers) == 1 {
			containerSuffix = containers[0]
			logrus.Debugf("selected container: %v", containerSuffix)
		} else {
			prompt := &survey.Select{
				Message: "select container",
				Options: containers,
			}
			survey.AskOne(prompt, &containerSuffix)
			if containerSuffix == "" {
				return ""
			}
		}

	}

	var containerFullName = serviceFullName + "." + containerSuffix

	return containerFullName
}
