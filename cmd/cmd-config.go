package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	yaml "gopkg.in/yaml.v2"
)

func CmdConfig(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "config",
		Short: "read config key",
		Args:  cobra.RangeArgs(0, 1),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()
			var bytes []byte
			var err error
			bytes, err = json.Marshal(cfg)
			errors.Check(err)

			if len(args) == 0 {
				fmt.Println(string(bytes))
			} else {
				config := make(map[interface{}]interface{})
				err = yaml.Unmarshal(bytes, config)
				errors.Check(err)
				key := args[0]
				val := config[key]
				switch val.(type) {
				case string:
					fmt.Println(val.(string))
				default:
					bytes, err = json.Marshal(val)
					errors.Check(err)
					fmt.Println(string(bytes))
				}
			}

		},
	}
	return cmd
}
