package cmd

import (
	"fmt"
	"os/exec"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/prompts"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdRm(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:     "rm [stacks]",
		Short:   "remove docker stack",
		Aliases: []string{"remove"},
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()

			var stackNames []string
			if len(args) > 0 {
				stackNames = args
			} else {
				stackNames = prompts.StacksMultiSelectFull(app, cmd, `rm all stacks %v ?`, "yes")
			}
			if len(stackNames) == 0 {
				return
			}

			runningStacks, _ := docker.StacksPrefixed(cfg.StackNamespace)
			for _, stack := range stackNames {
				if !tools.SliceContainsString(runningStacks, config.PrefixNS(stack, cfg.StackNamespace)) {
					logrus.Infof("stack %v not running", stack)
				} else {
					logrus.Infof("rm stack %v", stack)

					command := exec.Command("docker", "stack", "rm", config.PrefixNS(stack, cfg.StackNamespace))

					logStreamer, logStreamerClose := tools.LogStreamerFields(logrus.Fields{
						"stack": stack,
					})
					defer logStreamerClose()
					command.Stdout = logStreamer
					command.Stderr = logStreamer
					err := command.Run()
					errors.Check(err)
				}
			}

			if !cfg.RmDetach {
				wg := &sync.WaitGroup{}
				for _, stack := range stackNames {
					wg.Add(1)
					go func(stack string) {
						defer wg.Done()
						docker.StackWaitForRessourcesReleased(config.PrefixNS(stack, cfg.StackNamespace))
					}(stack)
				}
				wg.Wait()
			}

			if cfg.RmVolumes {
				wg := &sync.WaitGroup{}
				for _, stack := range stackNames {
					wg.Add(1)
					go func(stack string) {
						defer wg.Done()
						docker.VolumesByStackRemove(config.PrefixNS(stack, cfg.StackNamespace))
					}(stack)
				}
				wg.Wait()
			}

		},
	}

	flags := cmd.Flags()
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "rm all"))
	flags.BoolP("detach", "", config.FlagRmDetachDefault, config.FlagRmDetachDesc)
	flags.BoolP("volumes", "v", config.FlagRmVolumesDefault, config.FlagRmVolumesDesc)
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)

	return cmd
}
