package cmd

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
)

func CmdSh(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "sh",
		Short: "exec -it sh",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()

			argsP := &args

			containerFullName := CommonExecContainerName(app, cmd, argsP)

			var bashAvailable bool
			if cfg.ShBash {
				command := CommonExecCommand(app, cmd, &[]string{"bash"}, containerFullName)
				if command != nil {
					if err := command.Run(); err == nil {
						bashAvailable = true
					}
				}
			}

			var shellCmd string
			if cfg.ShBash && bashAvailable {
				shellCmd = "bash"
			} else {
				shellCmd = "sh"
			}

			args = *argsP
			args = append(args, []string{"--", "-it", shellCmd}...)
			argsP = &args

			command := CommonExecCommand(app, cmd, argsP, containerFullName)
			if command == nil {
				return
			}
			command.Stdout = os.Stdout
			command.Stderr = os.Stderr
			command.Stdin = os.Stdin
			if err := command.Run(); err != nil {
				logrus.Fatal(err)
			}

		},
	}

	flags := cmd.Flags()

	flags.SetInterspersed(false)

	flags.BoolP("bash", "b", config.FlagShBashDefault, config.FlagShBashDesc)

	return cmd
}
