package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
)

func CmdLsServices(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "services",
		Short: "display list of running services names",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			cfg := app.GetConfig()
			var namespace string
			if cfg.Stack != "" {
				namespace = config.PrefixNS(cfg.Stack, cfg.StackNamespace)
			} else {
				namespace = cfg.StackNamespace
			}
			var serviceNames []string
			var err error
			serviceNames, err = docker.ServicesPrefixed(namespace)
			errors.Check(err)
			if cfg.LsServicesShort && namespace != "" {
				serviceNamesShort := make([]string, len(serviceNames))
				l := len(namespace) + 1
				for i, serviceName := range serviceNames {
					serviceNamesShort[i] = serviceName[l:]
				}
				serviceNames = serviceNamesShort
			}
			fmt.Println(strings.Join(serviceNames, " "))
		},
	}
	flags := cmd.Flags()
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.BoolP("short", "", config.FlagLsServicesShortDefault, config.FlagLsServicesShortDesc)
	return cmd
}
