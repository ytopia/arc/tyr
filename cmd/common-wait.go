package cmd

import (
	"os/exec"
	"strings"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/youtopia.earth/ops/tyr/bundle"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/environ"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

type CommonWaitType int

const (
	CommonWaitStack CommonWaitType = iota
	CommonWaitService
)

func CommonWaitServices(app App, serviceNames []string) {
	cfg := app.GetConfig()
	var services []string
	for _, serviceName := range serviceNames {
		if cfg.Stack != "" {
			stackFullname := config.PrefixNS(cfg.Stack, cfg.StackNamespace)
			allServices, err := docker.FullServicesByStack(stackFullname)
			errors.Check(err)
			if !tools.SliceContainsString(allServices, serviceName) {
				serviceName = stackFullname + "_" + serviceName
			}
		}
		services = append(services, serviceName)
	}
	CommonWait(app, CommonWaitService, services)
}

func CommonWaitStacks(app App, stackNames []string) {
	cfg := app.GetConfig()
	for i, name := range stackNames {
		stackNames[i] = config.PrefixNS(name, cfg.StackNamespace)
	}
	CommonWait(app, CommonWaitStack, stackNames)
}

func CommonWait(app App, waitType CommonWaitType, names []string) {

	if len(names) == 0 {
		return
	}

	cfg := app.GetConfig()

	var filterLabel string
	var waitTypeText string
	switch waitType {
	case CommonWaitService:
		filterLabel = `com.docker.swarm.service.name`
		waitTypeText = `service`
	case CommonWaitStack:
		filterLabel = `com.docker.stack.namespace`
		waitTypeText = `stack`
	}

	wgMain := sync.WaitGroup{}
	labelEnvKey := bundle.DeployLabelEnvKey(app, "hash.stack")
	stackByService := make(map[string]string)
	envMapByStack := make(map[string]map[string]string)
	for _, name := range names {
		wgMain.Add(1)

		var stackNameFull string
		var stackName string

		switch waitType {
		case CommonWaitStack:
			stackNameFull = name
		case CommonWaitService:
			if _, hasKey := stackByService[name]; !hasKey {
				if out, err := exec.Command("docker", "service", "inspect", "--format", `{{index .Spec.Labels "com.docker.stack.namespace"}}`, name).Output(); err != nil {
					logrus.Fatal(err)
				} else {
					stackNameFull = strings.TrimRight(string(out), "\n")
				}
				stackByService[name] = stackNameFull
			}
			stackNameFull = stackByService[name]
		}
		if len(cfg.StackNamespace) == 0 {
			stackName = stackNameFull
		} else {
			stackName = stackNameFull[len(cfg.StackNamespace)+1:]
		}

		if _, hasKey := envMapByStack[stackName]; !hasKey {
			envMapByStack[stackName] = environ.StackEnvMapFromBundle(app, stackName)
		}
		envMap := envMapByStack[stackName]

		hashStack := envMap[labelEnvKey]

		go func(name, hashStack string) {
			fields := logrus.Fields{
				"hashStack": hashStack,
				"type":      waitTypeText,
				"name":      name,
			}
			contextLogger := logrus.WithFields(fields)
			wg := sync.WaitGroup{}
			contextLogger.Debug("waiting stack to release old containers")
			var containerIDList []string
			if out, err := exec.Command("docker", "container", "ls", "-q", "--filter", `label=`+filterLabel+`=`+name).Output(); err != nil {
				logrus.Fatal(err)
			} else {
				containerIDList = tools.ByteLinesToSlice(out)
			}
			for _, containerID := range containerIDList {
				wg.Add(1)
				go func(containerID string) {
					var containerHashStack string
					if out, err := exec.Command("docker", "container", "inspect", "--format", `{{index .Config.Labels "`+cfg.LabelsNamespace+`.hash.stack"}}`, containerID).Output(); err != nil {
						logrus.Fatal(err)
					} else {
						containerHashStack = strings.TrimRight(string(out), "\n")
					}
					if containerHashStack != hashStack {
						contextLogger.Debugf("waiting container %v to exit", containerID)
						cmd := exec.Command("docker", "wait", containerID)
						cmd.Run()
						contextLogger.Debugf("container %v exited", containerID)
					}
					wg.Done()
				}(containerID)
			}
			wg.Wait()
			contextLogger.Debug("stack containers will be up to date")
			wgMain.Done()
		}(name, hashStack)
	}
	wgMain.Wait()
	logrus.Debug("all stacks containers will be up to date")

}
