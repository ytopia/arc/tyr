package cmd

import (
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdCreateConfig(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "config NAME file|-",
		Short: "create config with hash suffix from a file or stdin",
		Args:  cobra.MinimumNArgs(2),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			name := args[0]
			file := args[1]

			var err error

			var content []byte
			if file == "-" {
				content, err = ioutil.ReadAll(os.Stdin)
			} else {
				content, err = ioutil.ReadFile(file)
			}
			if err != nil {
				logrus.Fatal(err)
			}

			fullName := name + "_" + docker.HashImmutableBytes(content)

			command := exec.Command("docker", "config", "create", fullName, "-")

			logStreamer, logStreamerClose := tools.LogStreamer()
			defer logStreamerClose()
			command.Stdout = logStreamer
			command.Stderr = logStreamer

			command.Stdin = strings.NewReader(string(content))

			err = command.Run()
			if err != nil {
				logrus.Fatal(err)
			}

		},
	}

	return cmd
}
