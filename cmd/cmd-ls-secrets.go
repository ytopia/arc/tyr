package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
)

func CmdLsSecrets(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "secrets",
		Short: "display list of hashed secrets (root names)",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()

			prefix := config.PrefixNS("", cfg.SecretNamespace)
			prefixL := len(prefix)

			configNames := docker.SecretRootNames(prefix)

			for k := range configNames {
				configNames[k] = configNames[k][prefixL:]
			}

			fmt.Println(strings.Join(configNames, " "))
		},
	}

	flags := cmd.Flags()

	flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)

	return cmd
}
