package cmd

import (
	"github.com/spf13/cobra"
)

func CmdClean(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "clean",
		Short: "remove stack's unused ressources",
	}
	cmd.AddCommand(CmdCleanContainers(app))
	cmd.AddCommand(CmdCleanConfigs(app))
	cmd.AddCommand(CmdCleanSecrets(app))
	cmd.AddCommand(CmdCleanVolumes(app))
	return cmd
}
