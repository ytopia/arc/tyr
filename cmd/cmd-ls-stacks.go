package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
)

func CmdLsStacks(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "stacks",
		Short: "display list of config stack names",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			stackNames := compose.GetStackNames(app)
			fmt.Println(strings.Join(stackNames, " "))
		},
	}
	flags := cmd.Flags()
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	return cmd
}
