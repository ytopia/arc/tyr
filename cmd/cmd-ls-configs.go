package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
)

func CmdLsConfigs(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "configs",
		Short: "display list of hashed configs (root names)",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()

			prefix := config.PrefixNS("", cfg.ConfigNamespace)
			prefixL := len(prefix)

			configNames := docker.ConfigRootNames(prefix)

			for k := range configNames {
				configNames[k] = configNames[k][prefixL:]
			}

			fmt.Println(strings.Join(configNames, " "))
		},
	}

	flags := cmd.Flags()

	flags.String("config-namespace", "", config.FlagConfigNamespaceDesc)

	return cmd
}
