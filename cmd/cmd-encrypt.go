package cmd

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/crypto"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
)

func CmdEncrypt(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "encrypt [file|-] [CRYPTED_FILE]",
		Short: "encrypt file or stdin",
		Args:  cobra.RangeArgs(0, 2),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()
			cfg.LoadCryptoKey(true)

			var err error
			var content string
			if len(args) == 0 {
				if cfg.EncryptData == "" {
					logrus.Fatal("missing encrypt data, --data flag or -|file argument")
				}
				content = cfg.EncryptData
			} else if args[0] == "-" {
				var contentB []byte
				contentB, err = ioutil.ReadAll(os.Stdin)
				errors.Check(err)
				content = string(contentB)
			} else {
				var contentB []byte
				contentB, err = ioutil.ReadFile(args[0])
				errors.Check(err)
				content = string(contentB)
			}

			crypted := crypto.Encrypt(cfg.CryptoKey, content)

			if len(args) < 2 {
				fmt.Println(crypted)
				return
			}

			cryptedFile := args[1]
			err = ioutil.WriteFile(cryptedFile, []byte(crypted), 0644)
			errors.Check(err)

		},
	}
	flags := cmd.Flags()
	flags.StringP("crypto-key", "", config.FlagCryptoKeyDefault, config.FlagCryptoKeyDesc)
	flags.StringP("crypto-key-file", "", config.FlagCryptoKeyFileDefault, config.FlagCryptoKeyFileDesc)
	flags.StringP("data", "d", "", config.FlagEncryptDataDesc)
	return cmd
}
