package cmd

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	yaml "gopkg.in/yaml.v2"
)

func CmdLsConfigKeys(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "config-keys",
		Short: "display list of tyr config keys",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()
			var configKeys []string

			var bytes []byte
			var err error
			bytes, err = json.Marshal(cfg)
			errors.Check(err)
			config := make(map[interface{}]interface{})
			err = yaml.Unmarshal(bytes, config)
			errors.Check(err)

			for key := range config {
				configKeys = append(configKeys, key.(string))
			}

			fmt.Println(strings.Join(configKeys, " "))
		},
	}
	return cmd
}
