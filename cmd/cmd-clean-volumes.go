package cmd

import (
	"fmt"
	"sync"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/prompts"
)

func CmdCleanVolumes(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "volumes [stacks]",
		Short: "remove stack's volumes",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			var stackNames []string
			if len(args) > 0 {
				stackNames = args
			} else {
				stackNames = prompts.StacksMultiSelectFull(app, cmd, `delete volumes of all stacks %v ?`, "yes")
			}
			if len(stackNames) == 0 {
				return
			}

			cfg := app.GetConfig()

			wg := &sync.WaitGroup{}
			for _, stack := range stackNames {
				wg.Add(1)
				go func(stack string) {
					defer wg.Done()
					docker.VolumesByStackRemove(config.PrefixNS(stack, cfg.StackNamespace))
				}(stack)
			}
			wg.Wait()

		},
	}

	flags := cmd.Flags()
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "clean all volumes"))

	return cmd
}
