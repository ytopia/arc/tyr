package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
)

func CmdLsBuilds(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "builds",
		Short: "display list of config build names",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			cfg := app.GetConfig()

			var buildNames []string

			stack := cfg.Stack
			if stack != "" {
				buildNames = compose.StackGetBuildNames(app, stack)
			} else {
				buildNames = compose.GetBuildNames(app)
			}

			fmt.Println(strings.Join(buildNames, " "))
		},
	}

	flags := cmd.Flags()
	flags.StringP("builds-dir", "", config.FlagBuildsDirDefault, config.FlagBuildsDirDesc)
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.StringP("deploy-bundles-dir", "", config.FlagDeployBundlesDirDefault, config.FlagDeployBundlesDirDesc)
	flags.StringP("deploy-locks-dir", "", config.FlagDeployLocksDirDefault, config.FlagDeployLocksDirDesc)

	return cmd
}
