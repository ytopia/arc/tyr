package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/crypto"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdCreateSecret(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "secret NAME [file|-]",
		Short: "create secret with hash suffix from a file or stdin",
		Args:  cobra.RangeArgs(1, 2),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()

			name := args[0]

			var err error
			var content string

			var isRandom bool
			if len(args) == 2 {
				file := args[1]
				var contentB []byte
				if file == "-" {
					contentB, err = ioutil.ReadAll(os.Stdin)
					errors.Check(err)
				} else {
					contentB, err = ioutil.ReadFile(file)
					errors.Check(err)
				}
				content = string(contentB)
			} else {
				var err error
				content, err = crypto.Random(crypto.RandomConfig{
					Regex:  cfg.RandomRegex,
					Chars:  cfg.RandomChars,
					Size:   cfg.RandomSize,
					Prefix: cfg.RandomPrefix,
				})
				errors.Check(err)

				isRandom = true
			}

			if cfg.CreateSecretDisplay {
				fmt.Println(content)
			}

			fullName := config.PrefixNS(name+"_"+docker.HashImmutableBytes([]byte(content)), cfg.SecretNamespace)

			var labelsArgs []string
			if isRandom {
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random=true"}...)
				labelsArgs = append(labelsArgs, []string{"--label", cfg.LabelsNamespace + ".random.size=" + strconv.Itoa(cfg.RandomSize)}...)
			}

			commandArgs := []string{"secret", "create"}
			commandArgs = append(commandArgs, labelsArgs...)
			commandArgs = append(commandArgs, fullName)
			commandArgs = append(commandArgs, "-")
			command := exec.Command("docker", commandArgs...)

			logStreamer, logStreamerClose := tools.LogStreamer()
			defer logStreamerClose()
			command.Stderr = logStreamer

			command.Stdin = strings.NewReader(content)
			err = command.Run()
			errors.Check(err)

		},
	}

	flags := cmd.Flags()
	flags.BoolP("display", "d", config.FlagCreateSecretDisplayDefault, config.FlagCreateSecretDisplayDesc)
	flags.Int("random-size", config.FlagRandomSizeDefault, config.FlagRandomSizeDesc)
	flags.String("random-prefix", config.FlagRandomPrefixDefault, config.FlagRandomPrefixDesc)
	flags.String("random-chars", config.FlagRandomCharsDefault, config.FlagRandomCharsDesc)
	flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)

	return cmd
}
