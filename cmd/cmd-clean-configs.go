package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
)

func CmdCleanConfigs(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "configs [ConfigRootNames]",
		Short: "remove swarm's unused immutables configs",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()
			keepLatest := cfg.CleanConfigsKeepLatest

			var configSelectedNames []string
			for _, arg := range args {
				configSelectedNames = append(configSelectedNames, arg)
			}
			if len(configSelectedNames) == 0 {
				docker.CleanImmutableByPrefix(docker.ImmutableConfig, config.PrefixNS("", cfg.ConfigNamespace), keepLatest)
			} else {
				for _, configSelectedName := range configSelectedNames {
					docker.CleanImmutable(docker.ImmutableConfig, config.PrefixNS(configSelectedName, cfg.ConfigNamespace), keepLatest)
				}
			}

		},
	}

	flags := cmd.Flags()
	flags.BoolP("keep-latest", "", config.FlagKeepLatestDefault, config.FlagKeepLatestDesc)
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "clean all configs"))
	flags.String("config-namespace", "", config.FlagConfigNamespaceDesc)

	return cmd
}
