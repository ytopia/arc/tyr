package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
)

func NewCmd(app App) *cobra.Command {
	cmd := CmdRoot(app)

	cmd.AddCommand(CmdAnd(app, cmd))
	cmd.AddCommand(CmdCompletion(app, cmd))
	cmd.AddCommand(CmdBuild(app))
	cmd.AddCommand(CmdBundle(app))
	cmd.AddCommand(CmdClean(app))
	cmd.AddCommand(CmdCreate(app))
	cmd.AddCommand(CmdDecrypt(app))
	cmd.AddCommand(CmdEncrypt(app))
	cmd.AddCommand(CmdExec(app))
	cmd.AddCommand(CmdLogs(app))
	cmd.AddCommand(CmdLs(app))
	cmd.AddCommand(CmdPs(app))
	cmd.AddCommand(CmdPull(app))
	cmd.AddCommand(CmdRandom(app))
	cmd.AddCommand(CmdRebootDocker(app))
	cmd.AddCommand(CmdRotateRandom(app))
	cmd.AddCommand(CmdRm(app))
	cmd.AddCommand(CmdSh(app))
	cmd.AddCommand(CmdUp(app))
	cmd.AddCommand(CmdConfig(app))
	cmd.AddCommand(CmdWait(app))
	cmd.AddCommand(CmdReadSecret(app))
	// cmd.AddCommand(CmdWaitContainer(app))

	return cmd
}

func CmdRoot(app App) *cobra.Command {
	cl := app.GetConfigLoader()

	cmd := &cobra.Command{
		Use:                    "tyr",
		Short:                  "Docker stacks as directory 🦊",
		Long:                   "Docker stacks as directory, superset of commands for docker swarm 🦊",
		BashCompletionFunction: newBashCompletionFunc(cl),
	}

	configFile := app.GetConfigFile()

	pFlags := cmd.PersistentFlags()

	pFlags.StringVarP(configFile, "config", "", os.Getenv(cl.PrefixEnv("CONFIG")), config.FlagConfigDesc)
	pFlags.StringP("dev-tag", "", "dev-${"+cl.PrefixEnv("MACHINE_ID")+"}", config.FlagDevTagDesc)
	pFlags.StringP("env", "e", "", config.FlagEnvDesc)
	pFlags.StringP("log-level", "l", config.FlagLogLevelDefault, config.FlagLogLevelDesc)
	pFlags.StringP("log-type", "", config.FlagLogTypeDefault, config.FlagLogTypeDesc)
	pFlags.BoolP("log-force-colors", "", config.FlagLogForceColorsDefault, config.FlagLogForceColorsDesc)
	pFlags.StringP("machine-id", "", "", config.FlagMachineIdDesc)
	pFlags.StringP("stack", "s", "", config.FlagStackDesc)
	pFlags.StringP("cwd", "", "", config.FlagCWDDesc)
	pFlags.StringP("namespace", "", "", config.FlagNamespaceDesc)
	pFlags.StringP("stack-namespace", "", "", config.FlagStackNamespaceDesc)

	pFlags.SetAnnotation("stack", cobra.BashCompCustom, []string{"__tyr_get_stacks"})

	v := app.GetViper()

	v.BindPFlag("CONFIG", pFlags.Lookup("config"))
	v.BindPFlag("DEV_TAG", pFlags.Lookup("dev-tag"))
	v.BindPFlag("ENV", pFlags.Lookup("env"))
	v.BindPFlag("LOG_LEVEL", pFlags.Lookup("log-level"))
	v.BindPFlag("LOG_TYPE", pFlags.Lookup("log-type"))
	v.BindPFlag("LOG_FORCE_COLORS", pFlags.Lookup("log-force-colors"))
	v.BindPFlag("MACHINE_ID", pFlags.Lookup("machine-id"))
	v.BindPFlag("STACK", pFlags.Lookup("stack"))

	v.BindEnv("CONFIG")
	v.BindEnv("DEV_TAG")
	v.BindEnv("ENV")
	v.BindEnv("LOG_LEVEL")
	v.BindEnv("LOG_TYPE")
	v.BindEnv("LOG_FORCE_COLORS")
	v.BindEnv("MACHINE_ID")
	v.BindEnv("STACK")

	return cmd
}
