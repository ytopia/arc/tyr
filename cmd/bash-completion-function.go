package cmd

import "gitlab.com/youtopia.earth/ops/tyr/config"

func newBashCompletionFunc(cl *config.ConfigLoader) string {
	var bashCompletionFunc = `
  __tyr_get_stack(){
    local stackflag=$(echo "${words[@]}" | sed -E 's/.*(--stack[= ]+|-s +)([A-Za-z_]+).*/\2/')
  if [[ $stackflag = *" "* ]]; then
      stackflag="$` + cl.PrefixEnv("STACK") + `"
  fi
  echo "$stackflag"
  }

	__tyr_get_namespace(){
    local namespaceflag=$(echo "${words[@]}" | sed -E 's/.*(--namespace[= ]+|-s +)([A-Za-z_]+).*/\2/')
  if [[ $namespaceflag = *" "* ]]; then
      namespaceflag="$` + cl.PrefixEnv("NAMESPACE") + `"
  fi
	if [ "$namespaceflag" = "" ]; then
      namespaceflag=$(tyr config namespace)
  fi
  echo "$namespaceflag"
  }

  __tyr_get_services()
  {
    local stackflag=$(__tyr_get_stack)

    local out

    if [ "$stackflag" != "" ]; then
      if out=$(tyr -s "$stackflag" ls services --short); then
				COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
      fi
    else
      if out=$(tyr ls services); then
        COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
      fi
    fi
  }

  __tyr_get_containers(){
  local service_name=$1
  local stackflag=$(__tyr_get_stack)
  local namespaceflag=$(__tyr_get_namespace)

	local out

  if [ "$service_name" != "" ]; then
    local service
    if [ "$stackflag" == "" ]; then
      service=${service_name}
    else
      service=${stackflag}_${service_name}
    fi
		if [ "$namespaceflag" != "" ]; then
			service=${namespaceflag}_${service}
		fi

		local service_len=${#service}
		if out=$(docker container ls --format {{.Names}} --filter label=com.docker.swarm.service.name=${service}); then

      local service
      local serviceShortnames=()
      while IFS= read -r service
      do
        serviceShortnames+=( "${service:$((service_len+1))}" )
      done < <(printf '%s\n' "$out")

      local serviceShortnamesStr=$( IFS=$'\n'; echo "${serviceShortnames[*]}" )
      COMPREPLY=( $( compgen -W "${serviceShortnamesStr[*]}" -- "$cur" ) )
    fi
  else
    if out=$(docker container ls --format {{.Names}}); then
      COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
    fi
  fi
  }

  __tyr_get_services_containers()
  {
  if [[ ${#nouns[@]} -eq 0 ]]; then
    __tyr_get_services
  else
    __tyr_get_containers ${nouns[${#nouns[@]} -1]}
  fi
  }

  __tyr_get_stacks()
  {
  local out
  if out=$(tyr ls stacks); then
    COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
  fi
  }

	__tyr_get_ls_config_keys()
	{
	local out
	if out=$(tyr ls config-keys); then
		COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
	fi
	}

  __tyr_get_stack_services_from_bundle()
  {
  local stackflag=$(__tyr_get_stack)
  if [ "$stackflag" != "" ]; then
    #__tyr_get_services
    local out=$(docker-compose -f bundles/${stackflag}.yml config --services 2>/dev/null)
    COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
  else
    COMPREPLY=( $( compgen -W "--stack" -- "$cur" ) )
  fi
  }

  __tyr_get_build_ls(){
  local stackflag=$(__tyr_get_stack)
  local out
  if [ "$stackflag" != "" ]; then
    out=$(tyr -s "$stackflag" ls builds)
  else
    out=$(tyr ls builds)
  fi
  COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
  }

  __tyr_get_config_root_names(){
  local out=$(tyr ls configs)
  COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
  }

  __tyr_get_secret_root_names(){
  local out=$(tyr ls secrets)
  COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
  }

	__tyr_get_secret_names(){
		local out=$(docker secret ls --format {{.Name}} -q)
		COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
	}

  __tyr_custom_func() {
    case ${last_command} in
        tyr_logs)
            __tyr_get_services
            return
            ;;
        tyr_exec | tyr_bash | tyr_sh)
            __tyr_get_services_containers
            return
            ;;
        tyr_pull | tyr_bundle | tyr_up | tyr_ps | tyr_rm | tyr_clean_containers | tyr_clean_volumes)
          __tyr_get_stacks
          return
          ;;
        tyr_clean_configs)
          __tyr_get_config_root_names
          return
          ;;
        tyr_clean_secrets|tyr_rotate-random)
          __tyr_get_secret_root_names
          return
          ;;
        tyr_build)
          __tyr_get_build_ls
          return
          ;;
				tyr_config)
					__tyr_get_ls_config_keys
					return
					;;
				tyr_wait)
					__tyr_get_services
					return
					;;
				tyr_read-secret)
					__tyr_get_secret_names
					return
					;;
        *)
        ;;
    esac
  }`
	return bashCompletionFunc
}
