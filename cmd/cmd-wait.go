package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/bundle"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/prompts"
)

func CmdWait(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "wait [SERVICE...]",
		Short: "waits for a docker containers to match stack hash label",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			cfg := app.GetConfig()

			// select stacks
			var stackNames []string
			stackNames = prompts.StacksMultiSelectFull(app, cmd, `up all stacks %v ?`, "yes")
			if len(stackNames) == 0 {
				return
			}

			if cfg.Bundle {
				bundle.Stacks(app, stackNames, cfg.ForceBundle)
			}

			if len(args) == 0 {
				CommonWaitStacks(app, stackNames)
			} else {
				CommonWaitServices(app, args)
			}

		},
	}

	flags := cmd.Flags()

	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "deploy all"))
	flags.StringP("shared-file", "", config.FlagSharedFileDefault, config.FlagSharedFileDesc)
	flags.StringP("shared-env-dir", "", config.FlagSharedEnvDirDefault, config.FlagSharedEnvDirDesc)
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.StringP("deploy-bundles-dir", "", config.FlagDeployBundlesDirDefault, config.FlagDeployBundlesDirDesc)
	flags.StringP("deploy-locks-dir", "", config.FlagDeployLocksDirDefault, config.FlagDeployLocksDirDesc)
	flags.BoolP("bundle", "", config.FlagBundleDefault, config.FlagBundleDesc)
	flags.BoolP("bundle-validate", "", config.FlagBundleValidateDefault, config.FlagBundleValidateDesc)
	flags.BoolP("bundle-update-image", "", config.FlagBundleUpdateImageDefault, config.FlagBundleUpdateImageDesc)
	flags.BoolP("bundle-update-stack", "", config.FlagBundleUpdateStackDefault, config.FlagBundleUpdateStackDesc)
	flags.StringP("labels-namespace", "", config.FlagLabelsNamespaceDefault, config.FlagLabelsNamespaceDesc)
	flags.BoolP("force-bundle", "", false, config.FlagForceBundleDesc)

	return cmd
}
