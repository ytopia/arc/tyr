package cmd

import (
	"context"
	"io"
	"os"
	"os/exec"
	"os/signal"
	"sync"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/bundle"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdLogs(app App) *cobra.Command {

	var cmd = &cobra.Command{
		Use:   "logs",
		Short: "docker service logs",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			cfg := app.GetConfig()

			stack := cfg.Stack

			var services []string
			var opts []string
			for _, arg := range args {
				if arg[0:1] == "-" {
					opts = append(opts, arg)
				} else {
					services = append(services, arg)
				}
			}

			if cfg.LogsFollow {
				opts = append(opts, "--follow")
			}

			var stackNames []string
			if stack != "" {
				stackNames = append(stackNames, stack)
			} else {
				stackNames = compose.GetStackNames(app)
			}

			if cfg.Bundle {
				bundle.Stacks(app, stackNames, cfg.ForceBundle)
			}

			if len(services) == 0 {
				for _, stackName := range stackNames {
					serviceNames := compose.StackGetServiceNames(app, stackName)
					var serviceFullnames []string
					for _, serviceName := range serviceNames {
						serviceFullnames = append(serviceFullnames, config.PrefixNS(stackName, cfg.StackNamespace)+"_"+serviceName)
					}
					services = append(services, serviceFullnames...)
				}
			} else {
				if stack != "" {
					for i, service := range services {
						services[i] = config.PrefixNS(stack, cfg.StackNamespace) + "_" + service
					}
				}
			}

			grep := cfg.LogsGrep
			grepOpts := cfg.LogsGrepOpts

			wg := &sync.WaitGroup{}

			mainChan := make(chan os.Signal)
			signal.Notify(mainChan, syscall.SIGINT, syscall.SIGTERM)
			ctx, cancel := context.WithCancel(context.Background())

			for _, service := range services {
				wg.Add(1)
				go func(service string) {
					commandPath, lookErr := exec.LookPath("docker")
					if lookErr != nil {
						logrus.Fatal(lookErr)
					}
					dockerCommand := []string{"service", "logs"}
					dockerCommand = append(dockerCommand, opts...)
					dockerCommand = append(dockerCommand, service)
					cmd := exec.Command(commandPath, dockerCommand...)

					go func(cmd *exec.Cmd) {
						<-ctx.Done()
						if cmd.Process != nil {
							cmd.Process.Kill()
						}
						wg.Done()
					}(cmd)

					if grep != "" {

						var grepArgs []string
						grepArgs = append(grepArgs, grep)
						if !tools.SliceContainsStringPrefix(grepOpts, "--color") {
							grepOpts = append(grepOpts, "--color=always")
						}
						grepArgs = append(grepArgs, grepOpts...)

						c2 := exec.Command("grep", grepArgs...)

						c2.Stdout = os.Stdout
						c2.Stderr = os.Stderr

						c1 := cmd
						pr, pw := io.Pipe()
						c1.Stdout = pw
						c1.Stderr = pw
						c2.Stdin = pr
						c2.Stdout = os.Stdout
						c1.Start()
						c2.Start()
						go func(c1 *exec.Cmd, pw *io.PipeWriter) {
							defer pw.Close()
							c1.Wait()
						}(c1, pw)
						c2.Wait()

					} else {
						cmd.Stdout = os.Stdout
						cmd.Stderr = os.Stderr
						cmd.Start()
						cmd.Wait()
					}

				}(service)
			}

			<-mainChan
			cancel()
			wg.Wait()

		},
	}

	flags := cmd.Flags()

	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.BoolP("follow", "f", config.FlagFollowDefault, config.FlagFollowDesc)
	flags.StringP("grep", "g", config.FlagGrepDefault, config.FlagGrepDesc)
	flags.StringSlice("grep-opts", config.FlagGrepOptsDefault, config.FlagGrepOptsDesc)

	flags.StringP("deploy-bundles-dir", "", config.FlagDeployBundlesDirDefault, config.FlagDeployBundlesDirDesc)
	flags.StringP("deploy-locks-dir", "", config.FlagDeployLocksDirDefault, config.FlagDeployLocksDirDesc)
	flags.StringP("shared-file", "", config.FlagSharedFileDefault, config.FlagSharedFileDesc)
	flags.StringP("shared-env-dir", "", config.FlagSharedEnvDirDefault, config.FlagSharedEnvDirDesc)
	flags.StringP("compose-version", "", config.FlagComposeVersionDefault, config.FlagComposeVersionDesc)
	flags.VarP(&FlagValueBoolPlusAuto{Value: config.FlagServiceDevTagDefault}, "service-dev-tag", "", config.FlagServiceDevTagDesc)
	flags.BoolP("force-bundle", "", false, config.FlagForceBundleDesc)
	flags.BoolP("bundle", "", config.FlagBundleDefault, config.FlagBundleDesc)
	flags.BoolP("bundle-validate", "", config.FlagBundleValidateDefault, config.FlagBundleValidateDesc)
	flags.BoolP("bunlde-update-image", "", config.FlagBundleUpdateImageDefault, config.FlagBundleUpdateImageDesc)
	flags.BoolP("bundle-update-stack", "", config.FlagBundleUpdateStackDefault, config.FlagBundleUpdateStackDesc)
	flags.StringP("labels-namespace", "", config.FlagLabelsNamespaceDefault, config.FlagLabelsNamespaceDesc)

	flags.String("network-namespace", "", config.FlagNetworkNamespaceDesc)
	flags.String("volume-namespace", "", config.FlagVolumeNamespaceDesc)
	flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)
	flags.String("config-namespace", "", config.FlagConfigNamespaceDesc)

	flags.SetInterspersed(false)

	return cmd
}
