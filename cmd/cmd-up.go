package cmd

import (
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"
	"sync"

	cmap "github.com/orcaman/concurrent-map"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/bundle"
	"gitlab.com/youtopia.earth/ops/tyr/compose"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/docker"
	"gitlab.com/youtopia.earth/ops/tyr/environ"
	"gitlab.com/youtopia.earth/ops/tyr/errors"
	"gitlab.com/youtopia.earth/ops/tyr/goenv"
	"gitlab.com/youtopia.earth/ops/tyr/prompts"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdUp(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:     "up",
		Aliases: []string{"deploy"},
		Short:   "docker stack deploy",
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			cfg := app.GetConfig()
			cfg.LoadCryptoKey(false)

			// select stacks
			var stackNames []string
			if len(args) > 0 {
				stackNames = args
			} else {
				stackNames = prompts.StacksMultiSelectFull(app, cmd, `up all stacks %v ?`, "yes")
			}
			if len(stackNames) == 0 {
				return
			}

			// bundle
			var bundleStacks cmap.ConcurrentMap
			if cfg.Bundle {
				bundleStacks = bundle.Stacks(app, stackNames, cfg.ForceBundle)
			}

			// pull
			pull := cfg.UpPull

			if pull {
				pullForce := cfg.PullForce
				pullIgnoreTags := cfg.PullIgnoreTags
				pullIgnoreDevTag := cfg.PullIgnoreDevTag

				if pullIgnoreDevTag {
					pullIgnoreTags = append(pullIgnoreTags, cfg.DevTag)
				}

				var pullImages []string
				for _, stack := range stackNames {
					images := compose.StackRequiredImages(app, stack)
					for _, image := range images {
						parts := strings.Split(image, ":")
						if len(parts) > 1 {
							imageTag := parts[len(parts)-1]
							if !tools.SliceContainsString(pullIgnoreTags, imageTag) {
								pullImages = append(pullImages, image)
							}
						} else {
							pullImages = append(pullImages, image)
						}
					}
				}

				for _, pullImage := range pullImages {
					logrus.Infof("pull image: %v", pullImage)
				}

				docker.PullImages(pullImages, pullForce)
			}

			// build
			buildDeps := cfg.UpBuild
			buildMissingDeps := cfg.UpBuildMissing

			if buildDeps || buildMissingDeps {
				for _, stack := range stackNames {
					buildNames := compose.StackGetBuildNames(app, stack)

					buildCleanEnv := cfg.BuildCleanEnv

					var buildOpts []string
					for _, arg := range cfg.BuildParameters {
						if arg != "" {
							buildOpts = append(buildOpts, arg)
						}
					}

					buildTasks := cmap.New()
					wg := &sync.WaitGroup{}
					for _, buildName := range buildNames {
						wg.Add(1)
						buildOptions := &compose.BuildOptions{
							App:              app,
							Name:             buildName,
							CleanEnv:         buildCleanEnv,
							BuildDeps:        buildDeps,
							BuildMissingDeps: buildMissingDeps,
							CommandArgs:      buildOpts,
							Tasks:            buildTasks,
							IsDependency:     true,
						}
						go func(buildOptions *compose.BuildOptions) {
							defer wg.Done()
							compose.Build(buildOptions)
						}(buildOptions)
					}
					wg.Wait()

				}
			}

			//reset hash.image deploy label (after pull and build)
			if cfg.Bundle && cfg.BundleUpdateImage {
				for _, stack := range stackNames {
					envMap := environ.StackEnvMapFromBundle(app, stack)
					envStackChanged := false
					composeFileI, _ := bundleStacks.Get(stack)
					composeFile := composeFileI.(*compose.ComposeFile)
					for serviceName, service := range composeFile.Services {
						var image string
						image = service["image"].(string)
						var err error
						image, err = goenv.Expand(image, envMap)
						errors.Check(err)
						var hash string
						hash, _ = docker.ImageHash(image)

						labelKey := "hash.image"
						deployLabelEnvKey := bundle.DeployLabelEnvKey(app, labelKey+"_"+serviceName)
						if envMap[deployLabelEnvKey] != hash {
							envMap[deployLabelEnvKey] = hash
							envStackChanged = true
						}
					}
					if envStackChanged {
						err := bundle.EnvWrite(app, stack, envMap)
						errors.Check(err)
					}
				}
			}

			// create networks
			for _, stack := range stackNames {
				composeFile := compose.StackReadComposeFile(app, stack)
				for networkName, networkI := range composeFile.Networks {
					if networkI == nil {
						continue
					}
					network := networkI.(map[interface{}]interface{})
					if xTyrCreate, hasKey := network["x-tyr-create"]; !(hasKey && xTyrCreate.(bool)) {
						continue
					}
					if name, hasKey := network["name"]; hasKey {
						networkName = name.(string)
					}
					if docker.NetworkExists(networkName) {
						continue
					}

					var networkDriver string
					if driver, hasKey := network["x-tyr-create-driver"]; hasKey {
						networkDriver = driver.(string)
					} else if driver, hasKey := network["driver"]; hasKey {
						networkDriver = driver.(string)
					} else {
						networkDriver = "overlay"
					}

					commandArgs := []string{"network", "create"}
					commandArgs = append(commandArgs, []string{"--driver", networkDriver}...)

					for keyI, val := range network {
						key := keyI.(string)
						if len(key) >= 13 && key[0:13] == "x-tyr-create-" {
							paramKey := key[13:]
							if paramKey == "driver" {
								continue
							}
							var paramPrefix string
							if len(paramKey) == 1 {
								paramPrefix = "-"
							} else {
								paramPrefix = "--"
							}
							switch val.(type) {
							case bool:
								if val.(bool) {
									commandArgs = append(commandArgs, paramPrefix+paramKey)
								}
							case string:
								commandArgs = append(commandArgs, []string{paramPrefix + paramKey, val.(string)}...)
							case []string:
								for _, item := range val.([]string) {
									commandArgs = append(commandArgs, []string{paramPrefix + paramKey, item}...)
								}
							case []interface{}:
								for _, item := range val.([]interface{}) {
									commandArgs = append(commandArgs, []string{paramPrefix + paramKey, item.(string)}...)
								}
							}
						}
					}

					commandArgs = append(commandArgs, networkName)

					logrus.Infof(`creating network "` + networkName + `"`)
					command := exec.Command("docker", commandArgs...)

					logStreamer, logStreamerClose := tools.LogStreamer()
					defer logStreamerClose()
					command.Stdout = logStreamer
					command.Stderr = logStreamer

					if err := command.Run(); err != nil {
						logrus.Fatal(err)
					}

				}
			}

			// if prepare stop here, before deploy
			if cfg.UpPrepare {
				return
			}

			// deploy
			for _, stack := range stackNames {

				var err error
				var stackFileContent []byte
				stackFile := cfg.AbsDeployBundlesDir() + "/" + stack + ".yml"
				stackFileContent, err = ioutil.ReadFile(stackFile)
				errors.Check(err)

				compose.DeployStackContent(app, stack, stackFileContent)

			}

			if !cfg.UpDetach {
				CommonWaitStacks(app, stackNames)
			}

		},
	}

	flags := cmd.Flags()
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "deploy all"))
	flags.StringP("shared-file", "", config.FlagSharedFileDefault, config.FlagSharedFileDesc)
	flags.StringP("shared-env-dir", "", config.FlagSharedEnvDirDefault, config.FlagSharedEnvDirDesc)
	flags.StringP("deploy-dir", "", config.FlagDeployDirDefault, config.FlagDeployDirDesc)
	flags.StringP("deploy-bundles-dir", "", config.FlagDeployBundlesDirDefault, config.FlagDeployBundlesDirDesc)
	flags.StringP("deploy-locks-dir", "", config.FlagDeployLocksDirDefault, config.FlagDeployLocksDirDesc)
	flags.StringP("builds-dir", "", config.FlagBuildsDirDefault, config.FlagBuildsDirDesc)
	flags.VarP(&FlagValueBoolPlusAuto{Value: config.FlagServiceDevTagDefault}, "service-dev-tag", "", config.FlagServiceDevTagDesc)
	flags.BoolP("pull", "", config.FlagPullDefault, config.FlagPullDesc)
	flags.BoolP("pull-force", "", config.FlagPullForceDefault, config.FlagPullForceDesc)
	flags.StringSlice("pull-ignore-tags", config.FlagPullIgnoreTagsDefault, config.FlagPullIgnoreTagsDesc)
	flags.BoolP("pull-ignore-dev-tag", "", config.FlagPullIgnoreDevTagDefault, config.FlagPullIgnoreDevTagDesc)
	flags.BoolP("build", "", config.FlagBuildDefault, config.FlagBuildDesc)
	flags.BoolP("build-missing", "", config.FlagBuildMissingDefault, config.FlagBuildMissingDesc)
	flags.BoolP("build-clean-env", "", config.FlagBuildCleanEnvDefault, config.FlagBuildCleanEnvDesc)
	flags.StringSlice("build-parameters", config.FlagBuildParametersDefault, config.FlagBuildParametersDesc)
	flags.StringP("build-compose-file", "c", config.FlagBuildComposeFileDefault, config.FlagBuildCleanEnvDesc)
	flags.BoolP("prepare", "", config.FlagPrepareDefault, config.FlagPrepareDesc)
	flags.StringP("compose-version", "", config.FlagComposeVersionDefault, config.FlagComposeVersionDesc)
	flags.BoolP("bundle", "", config.FlagBundleDefault, config.FlagBundleDesc)
	flags.BoolP("bundle-validate", "", config.FlagBundleValidateDefault, config.FlagBundleValidateDesc)
	flags.BoolP("bundle-update-image", "", config.FlagBundleUpdateImageDefault, config.FlagBundleUpdateImageDesc)
	flags.BoolP("bundle-update-stack", "", config.FlagBundleUpdateStackDefault, config.FlagBundleUpdateStackDesc)
	flags.StringP("labels-namespace", "", config.FlagLabelsNamespaceDefault, config.FlagLabelsNamespaceDesc)
	flags.BoolP("force-bundle", "", true, config.FlagForceBundleDesc)
	flags.StringP("crypto-key", "", config.FlagCryptoKeyDefault, config.FlagCryptoKeyDesc)
	flags.StringP("crypto-key-file", "", config.FlagCryptoKeyFileDefault, config.FlagCryptoKeyFileDesc)

	flags.String("network-namespace", "", config.FlagNetworkNamespaceDesc)
	flags.String("volume-namespace", "", config.FlagVolumeNamespaceDesc)
	flags.String("secret-namespace", "", config.FlagSecretNamespaceDesc)
	flags.String("config-namespace", "", config.FlagConfigNamespaceDesc)

	flags.BoolP("detach", "", config.FlagUpDetachDefault, config.FlagUpDetachDesc)

	return cmd
}
