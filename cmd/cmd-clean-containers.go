package cmd

import (
	"fmt"
	"os/exec"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/youtopia.earth/ops/tyr/config"
	"gitlab.com/youtopia.earth/ops/tyr/prompts"
	"gitlab.com/youtopia.earth/ops/tyr/tools"
)

func CmdCleanContainers(app App) *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "containers [stacks]",
		Short: "remove service's exited containers",
		// Long:    ``,
		// Example: ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {

			var stackNames []string
			if len(args) > 0 {
				stackNames = args
			} else {
				stackNames = prompts.StacksMultiSelectFull(app, cmd, `delete exited containers of all stacks %v ?`, "yes")
			}
			if len(stackNames) == 0 {
				return
			}

			cfg := app.GetConfig()
			getExitedContainerList := func(stack string) []string {
				var exitedContainerList []string
				if out, err := exec.Command("docker", "container", "ls", "-q", "--filter", `label=com.docker.stack.namespace=`+config.PrefixNS(stack, cfg.StackNamespace), "--filter", "status=exited").Output(); err != nil {
					logrus.Fatal(err)
				} else {
					exitedContainerList = tools.ByteLinesToSlice(out)
				}
				return exitedContainerList
			}

			for _, stack := range stackNames {
				containerList := getExitedContainerList(stack)
				for _, container := range containerList {
					logrus.Infof("docker rm %v", container)
					command := exec.Command("docker", "rm", container)
					if err := command.Run(); err != nil {
						logrus.Error(err)
					}
				}
			}

		},
	}

	flags := cmd.Flags()
	flags.BoolP("yes", "y", config.FlagYesDefault, fmt.Sprintf(config.FlagYesDesc, "clean all containers"))

	return cmd
}
